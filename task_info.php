<?php
session_start();
include_once("dbconfig.php");
$id = $_SESSION['user_id'];
if (!isset($_SESSION['user_id'])) {
  header("Location: index.php");
}
if ($_SESSION['user_role'] == 'Subadmin') {
    header("Location: subadmin_index.php");
}
if (isset($_GET['logout'])) 
 {
    $result = mysqli_query($mysqli, "UPDATE `tbl_user` SET `user_status`= 'Offline' where `user_id` = $id");
    session_destroy();
    unset($_SESSION['user_id']);
    header("location: index.php");
 }
 //fetch profile
$query = mysqli_query($mysqli,"Select * from tbl_user where user_id = $id");
while($res = mysqli_fetch_array($query))
{
  $id = $res['user_id'];
  $fname = $res['user_fname'];  
  $lname = $res['user_lname'];
  $address = $res['user_add'];
  $email = $res['user_email'];
  $gender = $res['user_gender'];
  $number = $res['user_number'];
  $password = $res['user_pass'];
  $user_pic = $res['user_pic'];
}

if(isset($_GET['task_id']))
{
    $task_id = $_GET['task_id'];
    $fetch_transaction = mysqli_query($mysqli, "SELECT *
                FROM tbl_transaction
                INNER JOIN tbl_transactioninfo ON tbl_transaction.ti_id = tbl_transactioninfo.ti_id
                INNER JOIN tbl_subservice ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id
                INNER JOIN tbl_service ON tbl_subservice.service_id = tbl_service.service_id
                where tbl_transactioninfo.ti_id = $task_id");

    while($row = mysqli_fetch_array($fetch_transaction))
    {
    $transaction_id = $row['transaction_id'];
    $transaction_date = $row['transaction_date'];
    $transaction_earnings = $row['transaction_earnings'];
    $transaction_commission = $row['transaction_commission'];
    $promo_id = $row['promo_id'];
    $subservice = $row['subservice'];
    $service = $row['service'];
    $ti_id = $row['ti_id'];
    $task_date = $row['task_date'];
    $task_location = $row['task_location'];
    $task_instruction = $row['task_instruction'];
    $task_status = $row['task_status'];
    $customer_id = $row['customer_id'];
    $handyman_id = $row['handyman_id'];
    }

    $fetch_customer = mysqli_query($mysqli, "SELECT *
                FROM tbl_user WHERE user_id = $customer_id");
    while($cust_row = mysqli_fetch_array($fetch_customer))
    {
      $cust_id = $cust_row['user_id'];
      $cust_fullname = $cust_row['user_fname'].' '.$cust_row['user_lname'];
    }

    $fetch_handyman = mysqli_query($mysqli, "SELECT *
                FROM tbl_user WHERE user_id = $handyman_id");
    while($handy_row = mysqli_fetch_array($fetch_handyman))
    {
      $handy_id = $handy_row['user_id'];
      $handy_fullname = $handy_row['user_fname'].' '.$handy_row['user_lname'];
    }

    if($task_status=='Rescheduled')
    {
        $fetch_resched_transaction = mysqli_query($mysqli, "SELECT *
                FROM tbl_rescheduledtransac
                INNER JOIN tbl_transaction ON tbl_rescheduledtransac.transaction_id = tbl_transaction.transaction_id
                INNER JOIN tbl_transactioninfo ON tbl_transaction.ti_id = tbl_transaction.ti_id
                where tbl_transactioninfo.ti_id = $task_id");
        while($resched_row = mysqli_fetch_array($fetch_resched_transaction))
        {
          $rt_id = $resched_row['rt_id'];
          $rt_date = $resched_row['rt_date'];
          $rt_proposed_date = $resched_row['rt_proposed_date'];
        }
    }

    else if($task_status=='Cancelled')
    {
        $fetch_cancelled_transaction = mysqli_query($mysqli, "SELECT *
                FROM tbl_cancelledtransac
                INNER JOIN tbl_transaction ON tbl_cancelledtransac.transaction_id = tbl_transaction.transaction_id
                INNER JOIN tbl_transactioninfo ON tbl_transaction.ti_id = tbl_transaction.ti_id
                where tbl_transactioninfo.ti_id = $task_id");
        while($cancelled_row = mysqli_fetch_array($fetch_cancelled_transaction))
        {
          $ct_id = $cancelled_row['ct_id'];
          $ct_date = $cancelled_row['ct_date'];
        }
    }

    else if($task_status=='Completed')
    {
        $fetch_completed_transaction = mysqli_query($mysqli, "SELECT *
                FROM tbl_completedtransac
                INNER JOIN tbl_transaction ON tbl_completedtransac.transaction_id = tbl_transaction.transaction_id
                INNER JOIN tbl_transactioninfo ON tbl_transaction.ti_id = tbl_transaction.ti_id
                where tbl_transactioninfo.ti_id = $task_id");
        while($completed_row = mysqli_fetch_array($fetch_completed_transaction))
        {
          $cot_id = $completed_row['cot_id'];
          $cot_date = $completed_row['cot_date'];
        }
    }


}

?>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Transaction Info</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="SA/vendors/font-awesome/css/font-awesome.min.css">
    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

    <link rel="stylesheet" href="SA/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="SA/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
</head>
<body class="animsition">
    <div class="page-wrapper">
     <!-- MENU SIDEBAR-->
     <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/Service.You.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub">
                            <a class="js-arrow" href="admin_index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            
                        </li>
                       
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-group"></i>Members</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="admin_list.php">
                                    <i class="fas fa-caret-right"></i>Admin</a>
                                </li>
                                <li>
                                    <a href="subadmin_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Admin</a>
                                </li>
                                <li>
                                    <a href="user_list.php">
                                    <i class="fas fa-caret-right"></i>User</a>
                                </li>
                                
                                <li>
                                    <a href="handyman_list.php">
                                    <i class="fas fa-caret-right"></i>Handyman</a>
                                </li>
                            
                            </ul>
                        </li>
                        
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Services</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="service_list.php">
                                    <i class="fas fa-caret-right"></i>Main Services</a>
                                </li>
                                <li>
                                    <a href="subservices_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Services</a>
                                </li>
                                
                            
                            </ul>
                        </li>
                         <li>
                            <a href="reviews_list.php">
                                <i class="fas fa-pencil-square-o"></i>Reviews</a>
                        </li>
                        
                        <li>
                            <a href="reports_list.php">
                                <i class="fas fa-folder-open"></i>Reports</a>
                        </li>
                        
                         <li class="active has-sub">
                            <a href="task_list.php">
                                <i class="fas fa-briefcase"></i>Task</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Transaction</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="transaction_completed_list.php">
                                    <i class="fas fa-caret-right"></i>Completed Transaction</a>
                                </li>
                                <li>
                                    <a href="transaction_cancelled_list.php">
                                    <i class="fas fa-caret-right"></i>Cancelled Transaction</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="promo_list.php">
                                <i class="fas fa-briefcase"></i>Promos</a>
                        </li>
                        
                        <li>
                            <a href="notification_list.php">
                                <i class="fas fa-bell"></i>Notification</a>
                        </li>
                        <li>
                            <a href="warranty_list.php">
                                <i class="fa fa-file-text"></i>Warranty</a>
                        </li>
                        <li>
                            <a href="reason_list.php">
                                <i class="fas fa-cog"></i>Reason Database</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $fname;?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="admin_profile.php"><?php echo $fname;?> <?php echo $lname;?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $email;?></span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-money-box"></i>Billing</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="admin_index.php?logout='1'">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->           
           <!-- MAIN CONTENT-->       
           <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                            <div class="animsition">
                                <div class="page-wrapper">
                                          <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-header" style="text-align: center;">Task Information</div>
                                                    <div class="card-body card-block">
                                                        <form action="promo_addform.php"  class="col s12" method="post" id="register" enctype="multipart/form-data" onsubmit="return validation()">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Transaction ID</div>
                                                                    <p class="form-control"><?php echo $transaction_id?></p>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if($task_status=='Completed'){
                                                            echo "
                                                            <div class=\"form-group\">
                                                                <div class=\"input-group\">
                                                                    <div class=\"input-group-addon\">Transaction Date</div>      
                                                                    <p class=\"form-control\">$transaction_date</p>
                                                                </div>
                                                            </div>
                                                                
                                                             <div class=\"form-group\">
                                                                <div class=\"input-group\">
                                                                    <div class=\"input-group-addon\">Transaction Earnings</div>
                                                                    <p class=\"form-control\">PHP $transaction_earnings</p>
                                                                </div>
                                                            </div>
                                                            <div class=\"form-group\">
                                                                <div class=\"input-group\">
                                                                    <div class=\"input-group-addon\">Transaction Commission</div>
                                                                    <p class=\"form-control\">PHP $transaction_commission</p>
                                                                </div>
                                                            </div>"; 
                                                                if($promo_id!=NULL){
                                                                    echo "<div class=\"form-group\">";
                                                                    echo "<div class=\"input-group\">";
                                                                    echo "<div class=\"input-group-addon\">Promo ID</div>";
                                                                    echo "<p class=\"form-control\">$promo_id</p>
                                                                        </div>
                                                                    </div>";
                                                            }
                                                            }
                                                            ?>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Service</div>
                                                                    <p class="form-control"><?php echo $subservice?>-<?php echo $service?></p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Task Date</div>
                                                                    <p class="form-control"><?php echo $task_date?></p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Task Location</div>
                                                                    <p class="form-control"><?php echo $task_location?></p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Task Instruction</div>
                                                                </div>
                                                                    <p class="form-control" style="height: 100px;"><?php echo $task_instruction?></p>
                                                            </div>
                                                            <?php
                                                            if($task_status=='Cancelled'){
                                                                echo "<div class=\"form-group\">";
                                                                echo "<div class=\"input-group\">";
                                                                echo "<div class=\"input-group-addon\">Task Status</div>";
                                                                echo "<p class=\"form-control\">$task_status-$ct_date</p>
                                                                    </div>
                                                                </div>";
                                                            }
                                                            else if($task_status=='Rescheduled'){
                                                                echo "
                                                                <div class=\"form-group\">
                                                                    <div class=\"input-group\">
                                                                        <div class=\"input-group-addon\">Task Status</div>
                                                                        <p class=\"form-control\">$task_status-$rt_date</p>
                                                                    </div>
                                                                </div>
                                                                <div class=\"form-group\">
                                                                    <div class=\"input-group\">
                                                                        <div class=\"input-group-addon\">Reschedule Date</div>
                                                                        <p class=\"form-control\">$rt_proposed_date</p>
                                                                    </div>
                                                                </div>"
                                                                ;
                                                            }
                                                            else if($task_status=='Completed'){
                                                                echo "
                                                                <div class=\"form-group\">
                                                                    <div class=\"input-group\">
                                                                        <div class=\"input-group-addon\">Task Status</div>
                                                                        <p class=\"form-control\">$task_status-$cot_date</p>
                                                                    </div>
                                                                </div>";
                                                            }
                                                            else{
                                                                echo "
                                                                <div class=\"form-group\">
                                                                    <div class=\"input-group\">
                                                                        <div class=\"input-group-addon\">Task Status</div>
                                                                        <p class=\"form-control\">$task_status</p>
                                                                    </div>
                                                                </div>";
                                                            }
                                                            ?>

                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Customer</div>
                                                                    <p class="form-control"><?php echo $cust_id?> - <?php echo $cust_fullname?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Handyman</div>
                                                                    <p class="form-control"><?php echo $handy_id?> - <?php echo $handy_fullname?></p>
                                                                </div>
                                                            </div>
                                                        </form>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>
    
    
    
    <script src="SA/vendors/jquery/dist/jquery.min.js"></script>
    <script src="SA/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="SA/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="SA/assets/js/main.js"></script>


    <script src="SA/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="SA/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="SA/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="SA/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="SA/vendors/jszip/dist/jszip.min.js"></script>
    <script src="SA/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="SA/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="SA/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="SA/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="SA/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="SA/assets/js/init-scripts/data-table/datatables-init.js"></script>


     <!-- Right Panel -->

    
</body>

</html>
<!-- end document-->
