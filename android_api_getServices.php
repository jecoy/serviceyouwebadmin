<?php 

header("Content-Type: application/json; charset=UTF-8");

require_once 'dbconfig.php';
$key = $_POST['key'];

if($key == null)
    {
    $result = mysqli_query($mysqli,"SELECT * from tbl_service where service_isActive = TRUE ");
    $response = array();

    while($row = mysqli_fetch_assoc($result)){

        array_push($response, 
        array(
            'service_id'=>$row['service_id'], 
            'service'=>$row['service'], 
            'service_icon'=>"http://192.168.43.65:8085/ServiceYou/images/servicepic/".$row['service_icon'])
        );
    }

    echo json_encode($response);
    mysqli_close($mysqli);
    }

else if($key == "selectHandymanServices"){
    $user_id = $_POST['user_id']; 
    $result = mysqli_query($mysqli,"SELECT * 
            from tbl_handymanservices 
            inner join tbl_subservice on tbl_handymanservices.subservice_id = tbl_subservice.subservice_id
            inner join tbl_service on tbl_subservice.service_id = tbl_service.service_id where tbl_handymanservices.user_id = $user_id");
    $response = array();

    while($row = mysqli_fetch_assoc($result)){

        array_push($response, 
        array(
            'user_id'=>$row['user_id'],
            'service_id'=>$row['service_id'],
            'subservice_id'=>$row['subservice_id'],  
            'service'=>$row['service'],
            'subservice'=>$row['subservice'], 
            'subservice_icon'=>"http://192.168.43.65:8085/ServiceYou/images/subservicepics/".$row['subservice_icon'],
            'subservice_rate'=>$row['subservice_rate'],
            'service_quickpitch'=>$row['service_quickpitch'],
            'service_levelofexp'=>$row['service_levelofexp']
            )
        );
    }

    echo json_encode($response);
    mysqli_close($mysqli);
    }

else if($key == "checkHmService")
    {
    $subservice_id = $_POST['subservice_id'];
    $user_id = $_POST['user_id'];

    $query = mysqli_query($mysqli,"SELECT * 
            from tbl_handymanservices 
            inner join tbl_subservice on tbl_handymanservices.subservice_id = tbl_subservice.subservice_id
            inner join tbl_service on tbl_subservice.service_id = tbl_service.service_id where tbl_handymanservices.user_id = $user_id && tbl_subservice.subservice_id = $subservice_id");
        if($query){
            $countResult = mysqli_num_rows($query);

            if($countResult>0){

                $result["value"] = "1";
                $result["message"] = "Existing Service!";

            }
            else{
                $result["value"] = "2";
                $result["message"] = "Non-existing Service!";
            }
        }
        else{
                $response["value"] = "0";
                echo json_encode($response);
                mysqli_close($mysqli);
        }

    echo json_encode($result);
    mysqli_close($mysqli);

    }

else if($key == "addHandymanService")
    {
    $subservice_id = $_POST['subservice_id'];
    $service_id = $_POST['service_id'];
    $user_id = $_POST['user_id'];
    $service_levelofexp = $_POST['service_levelofexp'];
    $service_quickpitch = $_POST['service_quickpitch'];

    $query = mysqli_query($mysqli,"INSERT INTO `tbl_handymanservices`(`user_id`,`service_id`, `subservice_id`, `service_quickpitch`, `service_levelofexp`, `hm_service_status`, `hm_service_isActive`) VALUES ('$user_id','$service_id','$subservice_id','$service_quickpitch','$service_levelofexp','Published',TRUE)");

        if($query)
        {
                $result["value"] = "1";
                $result["message"] = "Success!";
        }

        else{
                $response["value"] = "0";
                $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
                echo json_encode($response);
                mysqli_close($mysqli);
        }

    echo json_encode($result);
    mysqli_close($mysqli);

    }

else
    {
    $service_id = $_POST['service_id'];	
    $result = mysqli_query($mysqli,"SELECT * from tbl_subservice where service_id = '$service_id' && subservice_isActive = TRUE ");
    $response = array();

    while($row = mysqli_fetch_assoc($result)){
        array_push($response, 
        array(
            'subservice_id'=>$row['subservice_id'], 
            'service_id' => $row['service_id'],
            'subservice'=>$row['subservice'], 
            'subservice_icon'=>"http://192.168.43.65:8085/ServiceYou/images/subservicepics/".$row['subservice_icon'],
            'subservice_rate'=>$row['subservice_rate']
        	)
        );
    }
    echo json_encode($response);
    }
mysqli_close($mysqli);

?>

