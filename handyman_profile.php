<?php
session_start();
include_once("dbconfig.php");
$id = $_SESSION['user_id'];

if (!isset($_SESSION['user_id'])) {
  header("Location: index.php");
}
if (isset($_GET['logout'])) 
 {
    $result = mysqli_query($mysqli, "UPDATE `tbl_user` SET `user_status`= 'Offline' where `user_id` = $id");
    session_destroy();
    unset($_SESSION['user_id']);
    header("location: index.php");
 }
 //fetch profile
$query = mysqli_query($mysqli,"Select * from tbl_user where user_id = $id");
while($res = mysqli_fetch_array($query))
{
  $id = $res['user_id'];
  $fname = $res['user_fname'];  
  $lname = $res['user_lname'];
  $fullname = $res['user_fname'].' '.$res['user_lname'];
  $address = $res['user_add'];
  $email = $res['user_email'];
  $gender = $res['user_gender'];
  $number = $res['user_number'];
  $password = $res['user_pass'];
  $user_pic = $res['user_pic'];
  $role = $res['user_role'];
}
if(isset($_POST['submit']))
{
    $hm_points =  mysqli_real_escape_string($mysqli,$_POST['hm_points']);
    $handyman_id = $_SESSION['handyman_id'];  
    $today = date("Y-m-d");

    $select_user =  mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE user_id = $handyman_id");
    if($select_user){  
        while($row  = mysqli_fetch_array($select_user)){
            $hm_current_points = $row['user_points'];
        }

        $hm_new_points = $hm_points + $hm_current_points;
    }

    $addHM_points = mysqli_query($mysqli,"UPDATE `tbl_user` SET `user_points`= '$hm_new_points' WHERE user_id = $handyman_id");

    if($addHM_points)
    {        
        $add_points_update = mysqli_query($mysqli,"INSERT INTO `tbl_pointsupdate`(`user_id`, `points_amount`, `points_update_date`, `points_update_type`, `points_update_status`) VALUES ('$handyman_id','$hm_points','$today','Addition-Points','Published')");
        if($add_points_update)
        {
            echo "<script>alert('Succesfully added points.');window.location.href='http://localhost:8085/ServiceYou/handyman_profile.php?user_id=$handyman_id';</script>";
        }
        else
        {
            echo "<script>alert('Failed adding at table_pointsupdate');window.location.href='handyman_list.php';</script>";
        }
    }
    else
    {
        if($handyman_id==0){
            echo "<script>alert('Way sulod ang handyman ID');window.location.href='handyman_list.php';</script>";
        }
        else{
            echo "<script>alert('Failed adding');window.location.href='handyman_list.php';</script>";
        }
    }
}

if(isset($_GET['user_id']))
{
    $handyman_id = $_GET['user_id'];
    $_SESSION['handyman_id'] = $handyman_id;
    $fetch_user_profile = mysqli_query($mysqli, "SELECT *
                FROM tbl_user
                INNER JOIN tbl_hmprofile ON tbl_user.user_id = tbl_hmprofile.user_id
                INNER JOIN tbl_handymanlocation ON tbl_hmprofile.user_id = tbl_handymanlocation.user_id
                WHERE tbl_user.user_id = $handyman_id");

    while($row = mysqli_fetch_array($fetch_user_profile))
    {
      $handyman_id = $row['user_id'];
      $user_role = $row['user_role'];
      $user_fname = $row['user_fname'];  
      $user_lname = $row['user_lname'];
      $user_fullname = $row['user_fname'].' '.$row['user_lname'];
      $user_add = $row['user_add'];
      $user_email = $row['user_email'];
      $user_number = $row['user_number'];
      $user_pass = $row['user_pass'];
      $user_pp = $row['user_pic'];
      $user_points = $row['user_points'];

      $user_description = $row['user_description'];
      $user_workDescription = $row['user_workDescription'];
      $hms_location = $row['hms_location'];
      $hms_radius = $row['hms_radius'];
    }

    $fetch_user_transaction = mysqli_query($mysqli, "SELECT *
    FROM tbl_transactioninfo
    INNER JOIN tbl_subservice ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id
    INNER JOIN tbl_service ON tbl_subservice.service_id = tbl_service.service_id
    WHERE tbl_transactioninfo.handyman_id = $handyman_id");

    $fetch_user_warranty = mysqli_query($mysqli, "SELECT *
    FROM tbl_warranty
    INNER JOIN tbl_transaction ON tbl_warranty.transaction_id = tbl_transaction.transaction_id
    INNER JOIN tbl_transactioninfo ON tbl_transaction.ti_id = tbl_transactioninfo.ti_id
    WHERE tbl_transactioninfo.handyman_id = $handyman_id");

    $fetch_handyman_subservice = mysqli_query($mysqli,"SELECT * 
        from tbl_handymanservices 
        inner join tbl_subservice on tbl_handymanservices.subservice_id = tbl_subservice.subservice_id where user_id = $handyman_id");

    $fetch_handyman_docs = mysqli_query($mysqli,"SELECT * from tbl_handymandocs where user_id = $handyman_id");
}

// Program to display complete URL 
$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] 
                === 'on' ? "https" : "http") . "://" . 
          $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']; 
  
// Display the complete URL 
$_SESSION['last_url'] = $link;

?>
<!DOCTYPE html>
<html lang="en">

<head>
   <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Handyman Profile</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="SA/vendors/font-awesome/css/font-awesome.min.css">
    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

    <link rel="stylesheet" href="SA/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="SA/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <style>
    * {
      box-sizing: border-box;
    }

    /* Create two equal columns that floats next to each other */
    .column {
      float: left;
      width: 50%;
      padding: 10px;
      height: 300px; /* Should be removed. Only for demonstration */
    }
    /* Clear floats after the columns */
    .row:after {
      content: "";
      display: table;
      clear: both;
    }

#profileDisplay { display: block; height: 210px; width: 30%; margin: 0px auto; border-radius: 50%; }
.img-placeholder {
  width: 30%;
  color: white;
  height: 60%;
  background: black;
  opacity: .7;
  height: 210px;
  border-radius:50%;
  z-index: 2;
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  display: none;
}
.img-placeholder h4 {
  margin-top: 40%;
  color: white;
}
.img-div:hover .img-placeholder {
  display: block;
  cursor: pointer;
  background-color: white;
}
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
     <!-- MENU SIDEBAR-->
     <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/Service.You.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a class="js-arrow" href="admin_index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            
                        </li>
                       
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-group"></i>Members</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="admin_list.php">
                                    <i class="fas fa-caret-right"></i>Admin</a>
                                </li>
                                <li>
                                    <a href="subadmin_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Admin</a>
                                </li>
                                <li>
                                    <a href="user_list.php">
                                    <i class="fas fa-caret-right"></i>User</a>
                                </li>
                                
                                <li>
                                    <a href="handyman_list.php">
                                    <i class="fas fa-caret-right"></i>Handyman</a>
                                </li>
                            
                            </ul>
                        </li>
                        
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Services</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="service_list.php">
                                    <i class="fas fa-caret-right"></i>Main Services</a>
                                </li>
                                <li>
                                    <a href="subservices_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Services</a>
                                </li>
                                
                            
                            </ul>
                        </li>
                         <li>
                            <a href="reviews_list.php">
                                <i class="fas fa-pencil-square-o"></i>Reviews</a>
                        </li>
                        
                        <li>
                            <a href="reports_list.php">
                                <i class="fas fa-folder-open"></i>Reports</a>
                        </li>
                        
                         <li>
                            <a href="task_list.php">
                                <i class="fas fa-briefcase"></i>Task</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Transaction</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="transaction_completed_list.php">
                                    <i class="fas fa-caret-right"></i>Completed Transaction</a>
                                </li>
                                <li>
                                    <a href="transaction_cancelled_list.php">
                                    <i class="fas fa-caret-right"></i>Cancelled Transaction</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="earnings_list.php">
                                <i class="fas fa-dollar"></i> Earnings</a>
                        </li>
                        
                        <li>
                            <a href="promo_list.php">
                                <i class="fas fa-briefcase"></i>Promos</a>
                        </li>
                        
                        <li>
                            <a href="notification_list.php">
                                <i class="fas fa-bell"></i>Notification</a>
                        </li>
                        <li>
                            <a href="warranty_list.php">
                                <i class="fa fa-file-text"></i>Warranty</a>
                        </li>
                        <li>
                            <a href="reason_list.php">
                                <i class="fas fa-cog"></i>Reason Database</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $fname;?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix" style="background-color: transparent;">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="admin_profile.php"><?php echo $fname;?> <?php echo $lname;?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $email;?></span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-money-box"></i>Billing</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="admin_index.php?logout='1'">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">  
                                <div class="animated fadeIn">
                                        <!--================Home Banner Area =================-->
                                <section class="profile_area" style="padding-bottom: 20px;">
                                        <div class="profile_inner">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <img src="images/profilepic/<?php echo $user_pp;?>" alt="">
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="personal_text">
                                                        <h6>HELLO EVERYBODY, I AM</h6>
                                                        <h3><?php echo $user_fullname;?></h3>
                                                        <h4><?php echo $user_role;?></h4>
                                                        <div class="container-fluid">
                                                            <p><i class="fa fa-asterisk"></i> <?php echo $handyman_id?></p>
                                                            <p><i class="fa fa-phone"></i> <?php echo $user_number?> </p>
                                                            <p><i class="fa fa-envelope"></i> <?php echo $user_email?></p>
                                                            <p><i class="fa fa-location-arrow"></i> <?php echo $user_add?></p>
                                                            <p><i class="fa fa-money"></i> <?php echo $user_points?>&nbsp;
                                                                <button  type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#smallmodal">
                                                            <i class="fa fa-plus"></i>&nbsp;Points</button></p>                                                          
                                                            <p>Working Location</p>
                                                            <p><i class="fa fa-location-arrow"></i> <?php echo $hms_location?> within <?php echo $hms_radius?> Radius </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                </section>
                                <!--================End Home Banner Area =================-->
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">About my self.</strong>
                                    </div>
                                    <div class="card-body">
                                        <p><?php echo $user_description;?></p>
                                    </div> 
                                 </div>
                                  <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">About my work experience.</strong>
                                    </div>
                                    <div class="card-body">
                                        <p><?php echo $user_workDescription;?></p>
                                    </div> 
                                 </div>
                                <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Transactions Table</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Task ID</th>
                                                    <th>Service ID</th>
                                                    <th>Customer Name</th>
                                                    <th>Handyman Name</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    while($res = mysqli_fetch_array($fetch_user_transaction))
                                                    {
                                                        echo "<tr>";                                            
                                                        echo "<td>".$res['ti_id']."</td>";
                                                        echo "<td>".$res['subservice_id']."</td>";             
                                                        $custid = $res['customer_id'];
                                                        $customerinfo = mysqli_query($mysqli,"SELECT *
                                                                    FROM tbl_user where `user_id` = '$custid' ");    
                                                         while($cust = mysqli_fetch_array($customerinfo))
                                                         {
                                                            echo "<td>".$cust['user_fname']." ".$cust['user_lname']."</td>";
                                                         }

                                                        $handyid = $res['handyman_id'];
                                                        $handymaninfo = mysqli_query($mysqli,"SELECT *
                                                                    FROM tbl_user where `user_id` = '$handyid' "); 

                                                         while($handy = mysqli_fetch_array($handymaninfo))
                                                         {
                                                            echo "<td>".$handy['user_fname']." ".$handy['user_lname']."</td>";
                                                         }

                                                        echo "<td>".$res['task_status']."</td>";
                                                        echo "</tr>";                                            
                                                    }
                                                    ?> 
                                            </tbody>
                                        </table>
                            </div> 
                     </div>
                     <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Warranty Table</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Warranty ID</th>
                                                    <th>Transaction ID</th>
                                                    <th>Service ID</th>
                                                    <th>Customer Name</th>
                                                    <th>Handyman Name</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    while($res = mysqli_fetch_array($fetch_user_warranty))
                                                    {
                                                        echo "<tr>";                                            
                                                        echo "<td>".$res['warranty_id']."</td>";                         
                                                        echo "<td>".$res['transaction_id']."</td>";
                                                        echo "<td>".$res['subservice_id']."</td>";
                                                        $custid = $res['customer_id'];
                                                        $customerinfo = mysqli_query($mysqli,"SELECT *
                                                                    FROM tbl_user where `user_id` = '$custid' ");    
                                                         while($cust = mysqli_fetch_array($customerinfo))
                                                         {
                                                            echo "<td>".$cust['user_fname']." ".$cust['user_lname']."</td>";
                                                         }

                                                        $handyid = $res['handyman_id'];
                                                        $handymaninfo = mysqli_query($mysqli,"SELECT *
                                                                    FROM tbl_user where `user_id` = '$handyid' "); 

                                                         while($handy = mysqli_fetch_array($handymaninfo))
                                                         {
                                                            echo "<td>".$handy['user_fname']." ".$handy['user_lname']."</td>";
                                                         }

                                                        echo "<td>".$res['warranty_status']."</td>";
                                                        echo "</tr>";                                            
                                                    }
                                                    ?> 
                                            </tbody>
                                        </table>
                                    </div> 
                             </div>
                        </div>

                        <button type="button"  class="btn btn-outline-secondary" name="handyman_id" value="<?php echo"$handyman_id";?>" onclick="window.location.href='handyman_category_addform.php?handyman_id=<?php echo $handyman_id;?>'" >
                        <i class="fa fa-plus"></i>&nbsp; Add Services</button>
                        <div class="card">
                                            <div class="card-header">
                                                <strong class="card-title">Handyman Services Table</strong>
                                            </div>
                                            <div class="card-body">
                                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                                    <thead>
                                                    <tr>
                                                            <th>HMS ID</th>
                                                            <th>Subservice</th> 
                                                            <th>Rate</th>
                                                            <th>Pitch</th>
                                                            <th>Level of Experience</th> 
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                        while($res = mysqli_fetch_array($fetch_handyman_subservice))
                                                        {
                                                            echo "<tr>";                                    
                                                            echo "<td>".$res['hm_service_id']."</td>";                             
                                                            echo "<td>".$res['subservice']."</td>";
                                                            echo "<td>".$res['subservice_rate']."</td>";
                                                            echo "<td>".$res['service_quickpitch']."</td>";
                                                            echo "<td>".$res['service_levelofexp']."</td>";
                                                            /*echo "<td>
                                                            <a class='btn btn-info' href=\"edit_handymanservice.php?hm_service_id=$res[hm_service_id]\"><i class='fa fa-pencil fa-lg'></i></a>
                                                               <a href=\"delete_handymanservice.php?hm_service_id=$res[hm_service_id]\"  class=\"btn btn-danger confirmation\"><i class='fa fa-trash-o fa-lg'></i>
                                                                  </a></td>";*/
                                                            echo "</tr>";                                            
                                                        }
                                                        ?> 
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                        <button type="button"  class="btn btn-outline-secondary" name="handyman_id" value="<?php echo"$handyman_id";?>" onclick="window.location.href='handyman_documents_addform.php?handyman_id=<?php echo $handyman_id;?>'" >
                                        <i class="fa fa-plus"></i>&nbsp; Add Documents</button>
                                        <div class="card">
                                            <div class="card-header">
                                                <strong class="card-title">Documents</strong>
                                            </div>
                                            <div class="card-body">
                                                
                                                    <?php
                                                        while($res = mysqli_fetch_array($fetch_handyman_docs))
                                                        {
                                                            echo "<div class=\"row\">";                                          
                                                            echo "<div class=\"column\"><img src='images/documents/".$res['docu_pic']."' width='1200px' height='1200px''></div>";
                                                            echo "<div class=\"column\"><p>".$res['docu_name']."</p>";
                                                            echo "<p>".$res['docu_info']."</p>";
                                                           /* echo "<p> 
                                                            <a class='btn btn-info' href=\"edit_handymandocuments.php?service_id=$res[service_id]\"><i class='fa fa-pencil'></i></a>
                                                            <a href=\"delete_handymandocuments.php?id=$res[service_id]\"  class=\"btn btn-danger confirmation\"><i class='fa fa-trash-o fa-lg'></i>
                                                            </a></p></div>";*/
                                                           
                                                            echo "</div>";                                            
                                                        }
                                                        ?> 
                                            </div> 
                                        </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

    <!-- modal small -->
            <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="smallmodalLabel">Small Modal</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>  
                            <div class="modal-body">
                                <form action="handyman_profile.php"  class="col s12" method="post" id="addpoints">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">Points</div>
                                                    <input type="text" id="hm_points" name="hm_points" class="form-control" required>
                                                </div>
                                            </div>
                                        <div class="form-actions form-group">
                                            <button type="submit" name="submit" id="submitbtn" class="au-btn au-btn--block au-btn--green m-b-20">Confirm</button>
                                        </div>                                    
                                </form>  
                            </div>
                    </div>
                </div>
            </div>
            <!-- end modal small -->
</body>
 <script type="text/javascript">
    $('.confirmation').on('click', function () {
        return confirm('Are you sure?');
    });
    </script>
</html>
<!-- end document-->
