<?php 
header("Content-Type: application/json; charset=UTF-8");
require_once 'dbconfig.php';

  $key = $_POST['key'];

if ( $key == "insert" ){

  $fname = $_POST['user_fname'];
  $lname = $_POST['user_lname'];
  $password = $_POST['user_pass'];
  $email = $_POST['user_email'];
  $number = $_POST['user_number'];
  $address = $_POST['user_add'];
  $profilepic =  $_POST['user_pic'];
  $idpic =  $_POST['user_idPic'];
  $vcCode = $_POST['verification_code'];

    $query = "INSERT into tbl_user (user_fname,user_lname,user_add,user_number,user_email,user_pass,user_points,user_role,user_isActive,user_status) 
     values ('$fname','$lname','$address','$number','$email','$password','0','Customer','True','Offline')";

        if (mysqli_query($mysqli, $query) ){
            if ($profilepic == null){
                $getemail = mysqli_query($mysqli, "SELECT * FROM tbl_user WHERE user_email = '$email' "); 
                 while($res = mysqli_fetch_array($getemail))
                 {
                  $foreign_id = $res['user_id'];
                  $cust_number = $res['user_number'];
                 }

                $finalPath = "sylogo.png"; 
                $insert_picture = "UPDATE tbl_user SET user_pic='$finalPath' WHERE user_id='$foreign_id' ";

                if(mysqli_query($mysqli,$insert_picture)){

                    $insertProfile = "INSERT INTO `tbl_customerprofile`(`user_id`, `user_badge`, `user_idPic`) VALUES ('$foreign_id','Bronze','')";


                    $insert_vcCode = mysqli_query($mysqli,"INSERT INTO `tbl_verificationcode`(`user_id`, `verification_code`, `vc_status`)VALUES ('$foreign_id','$vcCode','Unverified')"); 

                    if(mysqli_query($mysqli, $insertProfile)){

                        if($idpic==null){
                            $finalPath = "sylogo.png"; 
                            $update_profile = "UPDATE tbl_customerprofile SET user_idPic='$finalPath' WHERE user_id='$foreign_id' ";
                            if(mysqli_query($mysqli, $update_profile)){
                            $result["value"] = "1";
                            $result["user_id"] = $foreign_id;
                            $result["message"] = "Success!"; 
                            }
                            else{
                                        $response["value"] = "0";
                                        $response["message"] = "Error at update_profile null id pic ".mysqli_error($mysqli);
                                        echo json_encode($response);
                                        mysqli_close($mysqli);
                            }
                        }
                        else{
                            $path = "images/idpics/$foreign_id.jpeg";
                            $update_profile = "UPDATE tbl_customerprofile SET user_idPic='$foreign_id.jpeg' WHERE user_id='$foreign_id' ";
                            if(mysqli_query($mysqli, $update_profile)&&file_put_contents( $path, base64_decode($idpic)))
                            {
                            $result["value"] = "1";
                            $result["user_id"] = $foreign_id;
                            $result["message"] = "Success!"; 
                            }
                            else{
                            $response["value"] = "0";
                            $response["message"] = "Error at update_profile w/ id pic".mysqli_error($mysqli);
                            echo json_encode($response);
                            mysqli_close($mysqli);
                            }
                        }
                    }

                    else{
                        $response["value"] = "0";
                        $response["message"] = "Error at inserting profile! ".mysqli_error($mysqli);
                        echo json_encode($response);
                        mysqli_close($mysqli);
                    }

                }

                else{
                    $response["value"] = "0";
                    $response["message"] = "Error at inserting picture! ".mysqli_error($mysqli);
                    echo json_encode($response);
                    mysqli_close($mysqli);
                }
            }
            else{
                $getemail = mysqli_query($mysqli, "SELECT * FROM tbl_user WHERE user_email = '$email' "); 
                 while($res = mysqli_fetch_array($getemail))
                 {
                  $foreign_id = $res['user_id'];
                  $cust_number = $res['user_number'];
                 }

                 $path = "images/profilepic/$foreign_id.jpeg";
                 $insert_picture = "UPDATE tbl_user SET user_pic='$foreign_id.jpeg' WHERE user_id='$foreign_id' ";
                 if(mysqli_query($mysqli,$insert_picture)&&file_put_contents( $path, base64_decode($profilepic))){

                    $insertProfile = mysqli_query($mysqli,"INSERT INTO `tbl_customerprofile`(`user_id`, `user_badge`) VALUES ('$foreign_id','Bronze')");


                    $insert_vcCode = mysqli_query($mysqli,"INSERT INTO `tbl_verificationcode`(`user_id`, `verification_code`, `vc_status`)VALUES ('$foreign_id','$vcCode','Unverified')"); 

                    if($insert_vcCode&&$insertProfile){
                        if($idpic==null){
                            $finalPath = "sylogo.png"; 
                            $update_profile = "UPDATE tbl_customerprofile SET user_idPic='$finalPath' WHERE user_id='$foreign_id' ";
                            if(mysqli_query($mysqli, $update_profile)){
                            $result["value"] = "1";
                            $result["user_id"] = $foreign_id;
                            $result["message"] = "Success!"; 
                            }
                            else{
                            $response["value"] = "0";
                            $response["message"] = "Error error at updating profile w/ null id pic w/ prof pic ".mysqli_error($mysqli);
                            echo json_encode($response);
                            mysqli_close($mysqli);
                            }
                        }

                        else{
                            $path = "images/idpics/$foreign_id.jpeg";
                            $update_profile = "UPDATE tbl_customerprofile SET user_idPic='$foreign_id.jpeg' WHERE user_id='$foreign_id' ";
                            if(mysqli_query($mysqli, $update_profile)&&file_put_contents( $path, base64_decode($idpic)))
                            {
                            $result["value"] = "1";
                            $result["user_id"] = $foreign_id;
                            $result["message"] = "Success!"; 
                            }
                            else{
                            $response["value"] = "0";
                            $response["message"] = "Error error at updating profile w/ id pic w/ prof pic ".mysqli_error($mysqli);
                            echo json_encode($response);
                            mysqli_close($mysqli);
                            }
                        }
                    }
                    else{
                        $response["value"] = "0";
                        $response["message"] = "Error at inserting profile with picture".mysqli_error($mysqli);
                        echo json_encode($response);
                        mysqli_close($mysqli);
                    }
                }

                else{
                    $response["value"] = "0";
                    $response["message"] = "Error at inserting picture ".mysqli_error($mysqli);
                    echo json_encode($response);
                    mysqli_close($mysqli);
                }
            }

        echo json_encode($result);
        mysqli_close($conn);
        }    

        else 
        {
             $response["value"] = "0";
             $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
             echo json_encode($response);
             mysqli_close($mysqli);
        }
}

else if( $key == "insertHandyman" ){

  $fname = $_POST['user_fname'];
  $lname = $_POST['user_lname'];
  $password = $_POST['user_pass'];
  $email = $_POST['user_email'];
  $number = $_POST['user_number'];
  $address = $_POST['user_add'];
  $profilepic =  $_POST['user_pic'];
  $vcCode = $_POST['verification_code'];

  $user_description = $_POST['user_description'];
  $user_workDescription = $_POST['user_workDescription'];
  $hms_location =  $_POST['hms_location'];

    $insertUser = mysqli_query($mysqli,"Insert into tbl_user (user_fname,user_lname,user_add,user_number,user_email,user_pass,user_pic,user_points,user_role,user_isActive,user_status) 
                                                    values ('$fname','$lname','$address','$number','$email','$password','$filename','0','Handyman','True','Offline')");

        if ($insertUser )
        {
          if ($profilepic == null)
          {
            $getemail = mysqli_query($mysqli, "SELECT * FROM tbl_user WHERE user_email = '$email' "); 
            while($res = mysqli_fetch_array($getemail))
            {
            $foreign_id = $res['user_id'];
            $cust_number = $res['user_number'];
            }
            $finalPath = "sylogo.png"; 
            $insert_picture = "UPDATE tbl_user SET user_pic='$finalPath' WHERE user_id='$foreign_id' ";
            if(mysqli_query($mysqli,$insert_picture))
            {
              $insertProfile = mysqli_query($mysqli,"INSERT INTO tbl_hmprofile (user_id,user_description,user_workDescription) VALUES ('$foreign_id','$user_description','$user_workDescription')");  


              $insert_vcCode = mysqli_query($mysqli,"INSERT INTO `tbl_verificationcode`(`user_id`, `verification_code`, `vc_status`)VALUES ('$foreign_id','$vcCode','Unverified')"); 

              $insertSchedule = mysqli_query($mysqli,"INSERT INTO `tbl_handymanlocation`(`user_id`, `hms_location`) VALUES ('$foreign_id','$hms_location')"); 

              if($insertProfile&&$insert_vcCode&&$insertSchedule)
              {
                $result["value"] = "1";
                $result["user_id"] = $foreign_id;
                $result["message"] = "Success!";

              }
              else
              {
                $response["value"] = "0";
                $response["message"] = "Error at inserting profile! ".mysqli_error($mysqli);
                echo json_encode($response);
                mysqli_close($mysqli);
              } 
            }
            else
            {
              $response["value"] = "0";
              $response["message"] = "Error at inserting picture! ".mysqli_error($mysqli);
              echo json_encode($response);
              mysqli_close($mysqli);
            }
          }

          else
          {
            $getemail = mysqli_query($mysqli, "SELECT * FROM tbl_user WHERE user_email = '$email' "); 
            while($res = mysqli_fetch_array($getemail))
            {
            $foreign_id = $res['user_id'];
            $cust_number = $res['user_number'];
            }

            $path = "images/profilepic/$foreign_id.jpeg";
            $insert_picture = "UPDATE tbl_user SET user_pic='$foreign_id.jpeg' WHERE user_id='$foreign_id' ";
            if(mysqli_query($mysqli,$insert_picture)&&file_put_contents( $path, base64_decode($profilepic)))
            {
              $insertProfile = mysqli_query($mysqli,"INSERT INTO tbl_hmprofile (user_id,user_description,user_workDescription) VALUES ('$foreign_id','$user_description','$user_workDescription')");  


              $insert_vcCode = mysqli_query($mysqli,"INSERT INTO `tbl_verificationcode`(`user_id`, `verification_code`, `vc_status`)VALUES ('$foreign_id','$vcCode','Unverified')"); 

              $insertSchedule = mysqli_query($mysqli,"INSERT INTO `tbl_handymanlocation`(`user_id`, `hms_location`) VALUES ('$foreign_id','$hms_location')");

              if($insertProfile&&$insert_vcCode&&$insertSchedule)
              {
                $result["value"] = "1";
                $result["user_id"] = $foreign_id;
                $result["message"] = "Success!";
              }
              else
              {
                $response["value"] = "0";
                $response["message"] = "Error at inserting profile! ".mysqli_error($mysqli);
                echo json_encode($response);
                mysqli_close($mysqli);
              }
            }
            else
            {
              $response["value"] = "0";
              $response["message"] = "Error at inserting picture! ".mysqli_error($mysqli);
              echo json_encode($response);
              mysqli_close($mysqli);
            }


          }

        echo json_encode($result);
        mysqli_close($conn); 
        }    

        else 
        {
             $response["value"] = "0";
             $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
             echo json_encode($response);
             mysqli_close($mysqli);
        }
  
}

else if( $key == "selectHandymanDocs"){
	$user_id = $_POST['user_id'];
	$getHandymanDocs = mysqli_query($mysqli,"SELECT * FROM tbl_handymandocs WHERE user_id = '$user_id'");
	$response = array();

	if($getHandymanDocs){
		while($res = mysqli_fetch_array($getHandymanDocs))
          {
          	array_push($response, 
	        array(
	            'user_id'=>$res['user_id'], 
	            'docu_pic'=>"http://192.168.43.65:8085/ServiceYou/images/documents/".$res['docu_pic'],
	            'docu_name' => $res['docu_name'],
	            'docu_info'=>$res['docu_info']
	        	)
	        );

          }
          echo json_encode($response);
	}

	else{
		$response["value"] = "0";
        $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
        echo json_encode($response);
        mysqli_close($mysqli);
	}
	mysqli_close($mysqli);
}

else if( $key == "insertHandymanDocs"){
	$user_id = $_POST['user_id'];
	$docu_pic = $_POST['docu_pic'];
	$docu_name = $_POST['docu_name'];
	$docu_info = $_POST['docu_info'];

	$query = mysqli_query($mysqli,"INSERT INTO `tbl_handymandocs` (`user_id`,`docu_name`,`docu_info`,`docu_isActive`) VALUES 
																 ('$user_id','$docu_name','$docu_info',TRUE)");
	if($query)
	{
		$path = "images/documents/$user_id.jpeg";
        $insert_docuPicture = "UPDATE tbl_handymandocs SET docu_pic='$user_id.jpeg' WHERE user_id='$user_id' ";
        if(mysqli_query($mysqli,$insert_docuPicture)&&file_put_contents( $path, base64_decode($docu_pic))){
        	$result["value"] = "1";
	        $result["message"] = "Success";
        }
        else{
	        $response["value"] = "0";
	        $response["message"] = "Error at inserting document picture ! ".mysqli_error($mysqli);
	        echo json_encode($response);
	        mysqli_close($mysqli);
        }
	}
	else{
		$response["value"] = "0";
        $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
        echo json_encode($response);
        mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if( $key == "select" ){

  $user_id =  $_POST['user_id'];
  $vcCode = $_POST['verification_code'];

  $query = mysqli_query($mysqli,"SELECT * FROM `tbl_verificationcode` WHERE user_id = '$user_id' && verification_code = '$vcCode'");
  if($query){
    $rowCount = mysqli_num_rows($query);
       if ($rowCount>0){
        $update_vc = mysqli_query($mysqli,"UPDATE `tbl_verificationcode` SET `vc_status`='Verified' WHERE user_id= '$user_id'");
        $selectUser = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE user_id= '$user_id'");
        while($res = mysqli_fetch_array($selectUser))
          {
            $user_id = $res['user_id'];
            $user_fname = $res['user_fname'];  
            $user_lname = $res['user_lname'];
            $user_add = $res['user_add'];
            $user_email = $res['user_email'];
            $user_gender = $res['user_gender'];
            $user_number = $res['user_number'];
            $user_pass = $res['user_pass'];
            $user_pic = $res['user_pic'];
            $user_role = $res['user_role'];
          }

        if($update_vc){
            $result["value"] = "1";
            $result["user_id"] = $user_id;
            $result["user_email"] = $user_email;
            $result["user_fname"] = $user_fname;
            $result["user_lname"] = $user_lname;
            $result["user_role"] = $user_role;
            $result["user_status"] = $user_status;
            $result["message"] = "Success!";
            $selectUser = mysqli_query($mysqli,"UPDATE `tbl_user` SET `user_status`='Online' WHERE user_id= '$user_id'");
        }

        else{
              $response["value"] = "0";
              $response["message"] = "Error at updating query ! ".mysqli_error($mysqli);
              echo json_encode($response);
              mysqli_close($mysqli);
        }


      }
      else{
        $result["value"] = "0";
        $result["message"] = "Failed in verifying, please check email again!";
      }

    echo json_encode($result);
    mysqli_close($mysqli);

  }
  else{

      $response["value"] = "0";
      $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
      echo json_encode($response);
      mysqli_close($mysqli);

  }
}

else if( $key == "login" ){
  $user_email =  $_POST['user_email'];
  $user_pass = $_POST['user_pass'];

  $selectUser = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE user_email = '$user_email' && user_pass = '$user_pass'");

  if($selectUser){
    $rowCount = mysqli_num_rows($selectUser);
       if ($rowCount>0)
       {
        $updateUser = mysqli_query($mysqli,"UPDATE `tbl_user` SET `user_status`='Online' WHERE user_id= '$user_id'");

          while($res = mysqli_fetch_array($selectUser))
          {
            $user_id = $res['user_id'];
            $user_fname = $res['user_fname'];  
            $user_lname = $res['user_lname'];
            $user_add = $res['user_add'];
            $user_email = $res['user_email'];
            $user_number = $res['user_number'];
            $user_pass = $res['user_pass'];
            $user_isActive = $res['user_isActive'];
            $user_pic = "http://192.168.43.65:8085/ServiceYou/images/profilepic/".$res['user_pic']; 
            $user_role = $res['user_role']; 

            if($user_role=='Handyman'){
            	$selectHandyman = mysqli_query($mysqli,"SELECT * FROM `tbl_hmprofile` WHERE `user_id` = '$user_id'");
            	while($res = mysqli_fetch_array($selectHandyman))
		          {
		            $user_description = $res['user_description'];
		            $user_workDescription = $res['user_workDescription'];  
		            $hm_efficieny = $res['hm_efficieny'];
		          }  
            }

            else if($user_role=='Customer'){
            	$selectCustomer= mysqli_query($mysqli,"SELECT * FROM `tbl_customerprofile` WHERE `user_id` = '$user_id'");
            	while($res = mysqli_fetch_array($selectCustomer))
		          {
		            $user_badge = $res['user_badge'];
		            $user_idPic = "http://192.168.43.65:8085/ServiceYou/images/profilepic/".$res['user_idPic'];  
		          }  
            }
          }                     

        $checkVerification = mysqli_query($mysqli,"SELECT * FROM `tbl_verificationcode`
                                                   WHERE user_id = '$user_id' && vc_status = 'Verified'");
          if($checkVerification){

            $rowCountVerification = mysqli_num_rows($checkVerification);
                  if($rowCountVerification > 0){
                    $result["user_id"] = $user_id;
                    $result["user_email"] = $user_email;
                    $result["user_fname"] = $user_fname;
                    $result["user_lname"] = $user_lname;
                    $result["user_role"] = $user_role;
                    $result["user_pic"]= $user_pic;
                    $result["user_add"]= $user_add;  
                    $result["user_number"]= $user_number;     


                    if($user_role=='Handyman'){	                    	
	                    $result["user_description"]= $user_description;  
	                    $result["user_workDescription"]= $user_workDescription;                    
	                    $result["hm_efficieny"] = $hm_efficieny;                       
                      $result["user_isActive"]= $user_isActive;  
                    }

                    else{
	                    $result["user_badge"]= $user_badge;                    
	                    $result["user_idPic"] = $user_idPic;
                    }

                    $result["value"] = "1";
                    $result["message"] = "Success!";
                  }
                  else{
                    $result["value"] = "2";
                    $result["user_id"] = $user_id;
                    $result["user_role"] = $user_role;
                    $result["message"] = "Unverified!";
                  }

          }

          else{
                $response["value"] = "0";
                $response["message"] = "Error at checking verification code query ! ".mysqli_error($mysqli);
                echo json_encode($response);
                mysqli_close($mysqli);
          }
        }

      else
      {
        $result["value"] = "0";
        $result["message"] = "Failed in logging in, please check email and password again!";
      }

    echo json_encode($result);
    mysqli_close($mysqli);

      }
  else{

      $response["value"] = "0";
      $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
      echo json_encode($response);
      mysqli_close($mysqli);

  }
}

else if( $key == "checkHandymanPointsForAvailability" ){
  $user_id =  $_POST['user_id'];

  $query = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE user_id= '$user_id'");
  if($query){
    $rowCount = mysqli_num_rows($query);
        while($res = mysqli_fetch_array($query))
          {
            $user_id = $res['user_id'];
            $user_points = $res['user_points'];  
          }


	        if($user_points >= 100){
	            $result["value"] = "1";
	            $result["message"] = "Success!";
	        }

	        else{
	              $result["value"] = "2";
	              $result["message"] = "Insufficient";
	        }

    echo json_encode($result);
    mysqli_close($mysqli);

  }

  else{

      $response["value"] = "0";
      $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
      echo json_encode($response);
      mysqli_close($mysqli);

  }
}

else if($key == "banHandyman"){
  $user_id = $_POST['user_id'];
  $selectUser = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE user_id = '$user_id'");

    if($selectUser){
        $updateUser = mysqli_query($mysqli,"UPDATE `tbl_user` SET `user_isActive`='FALSE' WHERE user_id= '$user_id'");
        if($updateUser){
              $result["value"] = "200:Banned";
              $result["message"] = "Success!";
        }
        else{
        $response["value"] = "0";
        $response["message"] = "Error at second query ! ".mysqli_error($mysqli);
        echo json_encode($response);
        mysqli_close($mysqli);
        }
    }
    else{
      $response["value"] = "0";
      $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
      echo json_encode($response);
      mysqli_close($mysqli);
    }

    echo json_encode($result);
    mysqli_close($mysqli);
}

else if( $key == "unbanHandyman" ){
  $user_id = $_POST['user_id'];
  $selectUser = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE user_id = '$user_id'");

    if($selectUser){
        $updateUser = mysqli_query($mysqli,"UPDATE `tbl_user` SET `user_isActive`='TRUE' WHERE user_id= '$user_id'");
        if($updateUser){
              $result["value"] = "200:Unbanned";
              $result["message"] = "Success!";
        }
        else{
        $response["value"] = "0";
        $response["message"] = "Error at second query ! ".mysqli_error($mysqli);
        echo json_encode($response);
        mysqli_close($mysqli);
        }
    }
    else{
      $response["value"] = "0";
      $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
      echo json_encode($response);
      mysqli_close($mysqli);
    }

    echo json_encode($result);
    mysqli_close($mysqli);
}

else if( $key == "checkHandyman" ){
  $user_id = $_POST['user_id'];
  $selectUser = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE user_id = '$user_id'");

    if($selectUser){
      while($res = mysqli_fetch_array($selectUser))
          {
            $user_id = $res['user_id'];
            $user_isActive = $res['user_isActive'];  
          }

        if($user_isActive=='true'){
              $result["value"] = "200:UserIsUnbanned";
              $result["user_isActive"] = $user_isActive;
        }

        else if($user_isActive=='false'){
              $result["value"] = "404:UserIsBanned";
              $result["user_isActive"] = $user_isActive;
        }

        else{
        $response["value"] = "0";
        $response["message"] = "Error at second query ! ".mysqli_error($mysqli);
        echo json_encode($response);
        mysqli_close($mysqli);
        }
    }
    else{
      $response["value"] = "0";
      $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
      echo json_encode($response);
      mysqli_close($mysqli);
    }

    echo json_encode($result);
    mysqli_close($mysqli);
}


else if($key=="getNotifications"){
  $user_id = $_POST['user_id'];
  $selectNotifications = mysqli_query($mysqli,"SELECT * FROM `tbl_notif` WHERE user_id = '$user_id' AND `notif_isSeen` = 'FALSE'");
  $result = array();
  if($selectNotifications){
    while($res = mysqli_fetch_array($selectNotifications))
          {
            array_push($result, 
            array(
                'notif_id'=>$res['notif_id'], 
                'user_id'=>$res['user_id'],
                'notif_subj' => $res['notif_subj'],
                'notif_date'=>$res['notif_date'],
                'notif_content'=>$res['notif_content'], 
                'notif_isSeen' => $res['notif_isSeen'],
                'notif_isActive '=>$res['notif_isActive']
              )
            );
          }
  }

  else{
    $response["value"] = "0";
        $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
        echo json_encode($response);
        mysqli_close($mysqli);
  }


  echo json_encode($result);
  mysqli_close($mysqli);
}

else if($key=="updateNotif"){
  $notif_id = $_POST['notif_id'];

  $updateNotification = mysqli_query($mysqli,"UPDATE `tbl_notif` SET `notif_isSeen`='TRUE' WHERE `notif_id`='$notif_id'");

  if($updateNotification){
      $result["value"]= "1";                    
      $result["message"] = "Success";
  }

  else{
    $response["value"] = "0";
        $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
        echo json_encode($response);
        mysqli_close($mysqli);
  }


  echo json_encode($result);
  mysqli_close($mysqli);
}

else{
  $user_email =  $_POST['user_email'];

  $selectUser = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE user_email = '$user_email'");
  if($selectUser){
    $rowCount = mysqli_num_rows($selectUser);
       if ($rowCount>0)
       {
          while($res = mysqli_fetch_array($selectUser))
          {
            $user_id = $res['user_id'];
            $user_fname = $res['user_fname'];  
            $user_lname = $res['user_lname'];
            $user_add = $res['user_add'];
            $user_email = $res['user_email'];
            $user_number = $res['user_number'];
            $user_pass = $res['user_pass'];
            $user_pic = "http://192.168.43.65:8085/ServiceYou/images/profilepic/".$res['user_pic']; 
            $user_role = $res['user_role'];

            if($user_role=='Handyman'){
            	$selectHandyman = mysqli_query($mysqli,"SELECT * FROM `tbl_hmprofile` WHERE `user_id` = '$user_id'");
            	while($res = mysqli_fetch_array($selectUser))
		          {
		            $user_description = $res['user_description'];
		            $user_workDescription = $res['user_workDescription'];  
		            $hm_efficieny = $res['hm_efficieny'];
		          }  
            }
            else{
            	$selectCustomer= mysqli_query($mysqli,"SELECT * FROM `tbl_customerprofile` WHERE `user_id` = '$user_id'");
            	while($res = mysqli_fetch_array($selectUser))
		          {
		            $user_badge = $res['user_badge'];
		            $user_idPic = "http://192.168.43.65:8085/ServiceYou/images/profilepic/".$res['user_idPic'];  
		          }  
            }
          }
          $result["user_id"] = $user_id;
          $result["user_email"] = $user_email;
          $result["user_fname"] = $user_fname;
          $result["user_lname"] = $user_lname;
          $result["user_role"] = $user_role;
          $result["user_pic"]= $user_pic;
          $result["user_add"]= $user_add;   
          $result["user_number"]= $user_number;     
          
          if($user_role=='Handyman'){	                    	
              $result["user_description"]= $user_description;  
              $result["user_workDescription"]= $user_workDescription;                    
              $result["hm_efficieny"] = $hm_efficieny;
          }

          else{
              $result["user_badge"]= $user_badge;                    
              $result["user_idPic"] = $user_idPic;
          }

          $result["value"] = "1";
          $result["message"] = "Success!";
        }
      else
      {
        $result["value"] = "0";
        $result["message"] = "Failed at sending,email is invalid!";
      }

    echo json_encode($result);
    mysqli_close($mysqli);
  }
  else{

      $response["value"] = "0";
      $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
      echo json_encode($response);
      mysqli_close($mysqli);

  }
}

?>