-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2019 at 06:15 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `serviceyoudb`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_temps`
--

CREATE TABLE `email_temps` (
  `email_id` int(11) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `email_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customerprofile`
--

CREATE TABLE `tbl_customerprofile` (
  `custProf_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_badge` varchar(50) NOT NULL,
  `user_idPic` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_handymanpoints`
--

CREATE TABLE `tbl_handymanpoints` (
  `hmp_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hmp_pointsAmt` double NOT NULL,
  `hmp_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_handymanschedule`
--

CREATE TABLE `tbl_handymanschedule` (
  `hms_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hms_quickpitch` varchar(100) NOT NULL,
  `hms_location` varchar(50) NOT NULL,
  `hms_radius` int(11) NOT NULL,
  `hms_day` varchar(50) NOT NULL,
  `hms_time` time NOT NULL,
  `subservice_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hmprofile`
--

CREATE TABLE `tbl_hmprofile` (
  `hmProf_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_description` varchar(1000) NOT NULL,
  `user_workDescription` varchar(1000) NOT NULL,
  `user_DOB` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notif`
--

CREATE TABLE `tbl_notif` (
  `notif_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notif_subj` varchar(50) NOT NULL,
  `notif_date` datetime NOT NULL,
  `notif_content` varchar(1000) NOT NULL,
  `notif_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_promos`
--

CREATE TABLE `tbl_promos` (
  `promo_id` int(11) NOT NULL,
  `promo_name` varchar(50) NOT NULL,
  `promo_code` varchar(50) NOT NULL,
  `promo_type` varchar(50) NOT NULL,
  `promo_amount` double NOT NULL,
  `promo_limit` varchar(50) NOT NULL,
  `promo_noOfUsed` int(11) NOT NULL,
  `promo_dateCreated` datetime NOT NULL,
  `promo_dateExpire` datetime NOT NULL,
  `promo_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ratesandreview`
--

CREATE TABLE `tbl_ratesandreview` (
  `rating_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating_date` datetime NOT NULL,
  `reviews` varchar(1000) DEFAULT NULL,
  `ratings` float DEFAULT NULL,
  `ratings_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reasons`
--

CREATE TABLE `tbl_reasons` (
  `reasons_id` int(11) NOT NULL,
  `reasons` varchar(100) NOT NULL,
  `reasons_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reports`
--

CREATE TABLE `tbl_reports` (
  `reports_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `report` varchar(1000) NOT NULL,
  `report_status` varchar(50) NOT NULL,
  `report_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rescheduledtransac`
--

CREATE TABLE `tbl_rescheduledtransac` (
  `rt_id` int(11) NOT NULL,
  `ti_id` int(11) NOT NULL,
  `rt_date` datetime NOT NULL,
  `rt_service_date` datetime NOT NULL,
  `reasons_id` int(11) NOT NULL,
  `rt_status` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `handyman_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service`
--

CREATE TABLE `tbl_service` (
  `service_id` int(11) NOT NULL,
  `service` varchar(50) NOT NULL,
  `service_status` varchar(50) NOT NULL,
  `service_icon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_service`
--

INSERT INTO `tbl_service` (`service_id`, `service`, `service_status`, `service_icon`) VALUES
(1, 'Carpentry', 'Published', '27750864_1570988686271961_4070734475578653138_n.jp'),
(2, 'Plumbing', 'Published', 'QAP.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subservice`
--

CREATE TABLE `tbl_subservice` (
  `subservice_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `subservice` varchar(50) NOT NULL,
  `subservice_status` varchar(50) NOT NULL,
  `subservice_info` varchar(50) NOT NULL,
  `subservice_icon` varchar(100) NOT NULL,
  `subservice_rate` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE `tbl_transaction` (
  `transaction_id` int(11) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `transaction_earnings` double NOT NULL,
  `transaction_commission` double NOT NULL,
  `promo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transactioninfo`
--

CREATE TABLE `tbl_transactioninfo` (
  `ti_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `subservice_id` int(11) NOT NULL,
  `service_date` datetime NOT NULL,
  `service_location` varchar(50) NOT NULL,
  `service_instruction` varchar(1000) NOT NULL,
  `service_status` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `handyman_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_fname` varchar(50) NOT NULL,
  `user_lname` varchar(50) NOT NULL,
  `user_add` varchar(50) NOT NULL,
  `user_number` varchar(50) NOT NULL,
  `user_email` varchar(50) DEFAULT NULL,
  `user_pass` varchar(50) NOT NULL,
  `user_pic` varchar(100) DEFAULT NULL,
  `user_points` float NOT NULL,
  `user_role` varchar(50) NOT NULL,
  `user_isActive` enum('true','false') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_fname`, `user_lname`, `user_add`, `user_number`, `user_email`, `user_pass`, `user_pic`, `user_points`, `user_role`, `user_isActive`) VALUES
(1, 'Jake', 'Liwag', 'LLC', '09238740222', 'jakelowieliwag@gmail.com', 'admin', 'service.png', 20, 'Admin', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_verificationcode`
--

CREATE TABLE `tbl_verificationcode` (
  `vc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `verification_code` varchar(50) NOT NULL,
  `vc_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_warranty`
--

CREATE TABLE `tbl_warranty` (
  `warranty_id` int(11) NOT NULL,
  `ti_id` int(11) NOT NULL,
  `warranty_status` varchar(50) NOT NULL,
  `warranty_expiration` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_warrantyinfo`
--

CREATE TABLE `tbl_warrantyinfo` (
  `warrantyInfo_id` int(11) NOT NULL,
  `warranty_id` int(11) NOT NULL,
  `warranty_date` datetime NOT NULL,
  `warranty_instruction` varchar(1000) NOT NULL,
  `warrantyInfo_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email_temps`
--
ALTER TABLE `email_temps`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `tbl_customerprofile`
--
ALTER TABLE `tbl_customerprofile`
  ADD PRIMARY KEY (`custProf_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_handymanpoints`
--
ALTER TABLE `tbl_handymanpoints`
  ADD PRIMARY KEY (`hmp_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_handymanschedule`
--
ALTER TABLE `tbl_handymanschedule`
  ADD PRIMARY KEY (`hms_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `subservice_id` (`subservice_id`);

--
-- Indexes for table `tbl_hmprofile`
--
ALTER TABLE `tbl_hmprofile`
  ADD PRIMARY KEY (`hmProf_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_notif`
--
ALTER TABLE `tbl_notif`
  ADD PRIMARY KEY (`notif_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_promos`
--
ALTER TABLE `tbl_promos`
  ADD PRIMARY KEY (`promo_id`);

--
-- Indexes for table `tbl_ratesandreview`
--
ALTER TABLE `tbl_ratesandreview`
  ADD PRIMARY KEY (`rating_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_reasons`
--
ALTER TABLE `tbl_reasons`
  ADD PRIMARY KEY (`reasons_id`);

--
-- Indexes for table `tbl_reports`
--
ALTER TABLE `tbl_reports`
  ADD PRIMARY KEY (`reports_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_rescheduledtransac`
--
ALTER TABLE `tbl_rescheduledtransac`
  ADD PRIMARY KEY (`rt_id`),
  ADD KEY `ti_id` (`ti_id`),
  ADD KEY `reasons_id` (`reasons_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `handyman_id` (`handyman_id`);

--
-- Indexes for table `tbl_service`
--
ALTER TABLE `tbl_service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `tbl_subservice`
--
ALTER TABLE `tbl_subservice`
  ADD PRIMARY KEY (`subservice_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `promo_id` (`promo_id`);

--
-- Indexes for table `tbl_transactioninfo`
--
ALTER TABLE `tbl_transactioninfo`
  ADD PRIMARY KEY (`ti_id`),
  ADD KEY `transaction_id` (`transaction_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `subservice_id` (`subservice_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `handyman_id` (`handyman_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- Indexes for table `tbl_verificationcode`
--
ALTER TABLE `tbl_verificationcode`
  ADD PRIMARY KEY (`vc_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_warranty`
--
ALTER TABLE `tbl_warranty`
  ADD PRIMARY KEY (`warranty_id`),
  ADD KEY `ti_id` (`ti_id`);

--
-- Indexes for table `tbl_warrantyinfo`
--
ALTER TABLE `tbl_warrantyinfo`
  ADD PRIMARY KEY (`warrantyInfo_id`),
  ADD KEY `warranty_id` (`warranty_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email_temps`
--
ALTER TABLE `email_temps`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_customerprofile`
--
ALTER TABLE `tbl_customerprofile`
  MODIFY `custProf_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_handymanpoints`
--
ALTER TABLE `tbl_handymanpoints`
  MODIFY `hmp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_handymanschedule`
--
ALTER TABLE `tbl_handymanschedule`
  MODIFY `hms_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_hmprofile`
--
ALTER TABLE `tbl_hmprofile`
  MODIFY `hmProf_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_notif`
--
ALTER TABLE `tbl_notif`
  MODIFY `notif_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_promos`
--
ALTER TABLE `tbl_promos`
  MODIFY `promo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_ratesandreview`
--
ALTER TABLE `tbl_ratesandreview`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_reasons`
--
ALTER TABLE `tbl_reasons`
  MODIFY `reasons_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_reports`
--
ALTER TABLE `tbl_reports`
  MODIFY `reports_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_rescheduledtransac`
--
ALTER TABLE `tbl_rescheduledtransac`
  MODIFY `rt_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_service`
--
ALTER TABLE `tbl_service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_subservice`
--
ALTER TABLE `tbl_subservice`
  MODIFY `subservice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_transactioninfo`
--
ALTER TABLE `tbl_transactioninfo`
  MODIFY `ti_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_verificationcode`
--
ALTER TABLE `tbl_verificationcode`
  MODIFY `vc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_warranty`
--
ALTER TABLE `tbl_warranty`
  MODIFY `warranty_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_warrantyinfo`
--
ALTER TABLE `tbl_warrantyinfo`
  MODIFY `warrantyInfo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_customerprofile`
--
ALTER TABLE `tbl_customerprofile`
  ADD CONSTRAINT `tbl_customerprofile_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_handymanpoints`
--
ALTER TABLE `tbl_handymanpoints`
  ADD CONSTRAINT `tbl_handymanpoints_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_handymanschedule`
--
ALTER TABLE `tbl_handymanschedule`
  ADD CONSTRAINT `tbl_handymanschedule_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`),
  ADD CONSTRAINT `tbl_handymanschedule_ibfk_2` FOREIGN KEY (`subservice_id`) REFERENCES `tbl_subservice` (`subservice_id`);

--
-- Constraints for table `tbl_hmprofile`
--
ALTER TABLE `tbl_hmprofile`
  ADD CONSTRAINT `tbl_hmprofile_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_notif`
--
ALTER TABLE `tbl_notif`
  ADD CONSTRAINT `tbl_notif_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_ratesandreview`
--
ALTER TABLE `tbl_ratesandreview`
  ADD CONSTRAINT `tbl_ratesandreview_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_reports`
--
ALTER TABLE `tbl_reports`
  ADD CONSTRAINT `tbl_reports_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_rescheduledtransac`
--
ALTER TABLE `tbl_rescheduledtransac`
  ADD CONSTRAINT `tbl_rescheduledtransac_ibfk_1` FOREIGN KEY (`ti_id`) REFERENCES `tbl_transactioninfo` (`ti_id`),
  ADD CONSTRAINT `tbl_rescheduledtransac_ibfk_2` FOREIGN KEY (`reasons_id`) REFERENCES `tbl_reasons` (`reasons_id`),
  ADD CONSTRAINT `tbl_rescheduledtransac_ibfk_3` FOREIGN KEY (`customer_id`) REFERENCES `tbl_user` (`user_id`),
  ADD CONSTRAINT `tbl_rescheduledtransac_ibfk_4` FOREIGN KEY (`handyman_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_subservice`
--
ALTER TABLE `tbl_subservice`
  ADD CONSTRAINT `tbl_subservice_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `tbl_service` (`service_id`);

--
-- Constraints for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD CONSTRAINT `tbl_transaction_ibfk_1` FOREIGN KEY (`promo_id`) REFERENCES `tbl_promos` (`promo_id`);

--
-- Constraints for table `tbl_transactioninfo`
--
ALTER TABLE `tbl_transactioninfo`
  ADD CONSTRAINT `tbl_transactioninfo_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `tbl_transaction` (`transaction_id`),
  ADD CONSTRAINT `tbl_transactioninfo_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `tbl_service` (`service_id`),
  ADD CONSTRAINT `tbl_transactioninfo_ibfk_3` FOREIGN KEY (`subservice_id`) REFERENCES `tbl_subservice` (`subservice_id`),
  ADD CONSTRAINT `tbl_transactioninfo_ibfk_4` FOREIGN KEY (`customer_id`) REFERENCES `tbl_user` (`user_id`),
  ADD CONSTRAINT `tbl_transactioninfo_ibfk_5` FOREIGN KEY (`handyman_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_verificationcode`
--
ALTER TABLE `tbl_verificationcode`
  ADD CONSTRAINT `tbl_verificationcode_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_warranty`
--
ALTER TABLE `tbl_warranty`
  ADD CONSTRAINT `tbl_warranty_ibfk_1` FOREIGN KEY (`ti_id`) REFERENCES `tbl_transactioninfo` (`ti_id`);

--
-- Constraints for table `tbl_warrantyinfo`
--
ALTER TABLE `tbl_warrantyinfo`
  ADD CONSTRAINT `tbl_warrantyinfo_ibfk_1` FOREIGN KEY (`warranty_id`) REFERENCES `tbl_warranty` (`warranty_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
