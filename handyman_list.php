<?php
session_start();
include_once("dbconfig.php");
$id = $_SESSION['user_id'];
if (!isset($_SESSION['user_id'])) {
  header("Location: index.php");
}
if ($_SESSION['user_role'] == 'Subadmin') {
    header("Location: subadmin_index.php");
}

if (isset($_GET['logout'])) 
 {         
    $result = mysqli_query($mysqli, "UPDATE `tbl_user` SET `user_status`= 'Offline' where `user_id` = $id");
    session_destroy();
    unset($_SESSION['user_id']);
    header("location: index.php");
 }
 //fetch profile
$query = mysqli_query($mysqli,"Select * from tbl_user where user_id = $id");
while($res = mysqli_fetch_array($query))
{
  $id = $res['user_id'];
  $fname = $res['user_fname'];  
  $lname = $res['user_lname'];
  $address = $res['user_add'];
  $email = $res['user_email'];
  $gender = $res['user_gender'];
  $number = $res['user_number'];
  $password = $res['user_pass'];
  $user_pic = $res['user_pic'];
}

$result = mysqli_query($mysqli,"Select * from tbl_user where user_role = 'Handyman' && user_isActive = true");

$count = mysqli_fetch_array(mysqli_query($mysqli, "SELECT COUNT(*) FROM tbl_user where user_role = 'Handyman'"));
$countActive = mysqli_fetch_array(mysqli_query($mysqli, "SELECT COUNT(*) FROM tbl_user where user_isActive = TRUE && user_role = 'Handyman'"));
$countInactive = mysqli_fetch_array(mysqli_query($mysqli, "SELECT COUNT(*) FROM tbl_user where user_isActive != TRUE && user_role = 'Handyman' "));
$countUnverified = mysqli_fetch_array(mysqli_query($mysqli, "SELECT COUNT(*) FROM tbl_user inner join tbl_verificationcode on tbl_user.user_id = tbl_verificationcode.user_id where tbl_verificationcode.vc_status = 'Unverified' && tbl_user.user_role = 'Handyman'"));

if(isset($_POST["generate_pdf"]))  
 {  
       require_once('pdfexport/tcpdf.php');  

       class MYPDF extends TCPDF {

          //Page header
          public function Header() {
              // Logo
              $image_file = K_PATH_IMAGES.'pdfexport/logo_example.jpg';
              $this->Image('pdfexport/logo_example.jpg', 95, 10, 20, '', 'JPG', '', 'C', false, 300, '', false, false, 0, false, false, false);
              // Set font
              $this->SetFont('helvetica', 'B', 20);
              // Title
              $this->Cell(0, 15, 'Service.YOU', 0, false, 'C', 0, '', 0, false, 'M', 'M');
          }

          // Page footer
          public function Footer() {
              // Position at 15 mm from bottom
              $this->SetY(-15);
              // Set font
              $this->SetFont('helvetica', 'I', 8);
              // Page number
              $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
          }
      }

      $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Nicola Asuni');
      $pdf->SetTitle('HANDYMAN LIST');
      $pdf->SetSubject('TCPDF Tutorial');
      $pdf->SetKeywords('TCPDF, PDF, example, test, guide');



      $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

      
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
      if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
          require_once(dirname(__FILE__).'/lang/eng.php');
          $pdf->setLanguageArray($l);
      }
      $pdf->SetFont('times', 'B', 12);

      // add a page
      $pdf->AddPage();  
      $content = '';  
      $content .= '  
      <div class="container">
      <h4 align="center">Handyman Data</h4><br /> 
      <table border="1" cellspacing="0" cellpadding="3">  
           <tr>  
                <th width="5%">Id</th>  
                <th width="30%">Name</th>  
                <th width="20%">Email</th>  
                <th width="15%">Number</th> 
                <th width="15%">Address</th> 
                <th width="15%">Role</th> 
           </tr>  
      ';  
      $content .= fetch_data();  
      $content .= '</table>';  
      $content .= '</div>';
      $pdf->writeHTML($content);  
      $pdf->Output('file.pdf', 'I');  
 }  
function fetch_data()  
 {  
      $output = '';  
      $conn = mysqli_connect("localhost", "root", "", "serviceyoudb");  
      $sql = "SELECT * FROM tbl_user WHERE `user_role` = 'Handyman' ORDER BY user_id ASC";  
      $result = mysqli_query($conn, $sql);  
      while($row = mysqli_fetch_array($result))  
      {       
      $output .= '<tr>  
                          <td>'.$row["user_id"].'</td>  
                          <td>'.$row["user_fname"].' '.$row["user_lname"].'</td>  
                          <td>'.$row["user_email"].'</td>  
                          <td>'.$row["user_number"].'</td>  
                          <td>'.$row["user_add"].'</td>  
                          <td>'.$row["user_role"].'</td>  
                     </tr>  
                          ';  
      }  
      return $output;  
 } 
?>

<html lang="en">

<head>
  <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Handyman</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="SA/vendors/font-awesome/css/font-awesome.min.css">
    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

    <link rel="stylesheet" href="SA/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="SA/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
</head>
<body class="animsition">
    <div class="page-wrapper">
     <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/Service.You.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a class="js-arrow" href="admin_index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            
                        </li>
                       
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-group"></i>Members</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="admin_list.php">
                                    <i class="fas fa-caret-right"></i>Admin</a>
                                </li>
                                <li>
                                    <a href="subadmin_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Admin</a>
                                </li>
                                <li>
                                    <a href="user_list.php">
                                    <i class="fas fa-caret-right"></i>User</a>
                                </li>
                                
                                <li>
                                    <a href="handyman_list.php">
                                    <i class="fas fa-caret-right"></i>Handyman</a>
                                </li>
                            
                            </ul>
                        </li>
                        
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Services</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="service_list.php">
                                    <i class="fas fa-caret-right"></i>Main Services</a>
                                </li>
                                <li>
                                    <a href="subservices_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Services</a>
                                </li>
                                
                            
                            </ul>
                        </li>
                         <li>
                            <a href="reviews_list.php">
                                <i class="fas fa-pencil-square-o"></i>Reviews</a>
                        </li>
                        
                        <li>
                            <a href="reports_list.php">
                                <i class="fas fa-folder-open"></i>Reports</a>
                        </li>
                        
                         <li>
                            <a href="task_list.php">
                                <i class="fas fa-briefcase"></i>Task</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Transaction</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="transaction_completed_list.php">
                                    <i class="fas fa-caret-right"></i>Completed Transaction</a>
                                </li>
                                <li>
                                    <a href="transaction_cancelled_list.php">
                                    <i class="fas fa-caret-right"></i>Cancelled Transaction</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="promo_list.php">
                                <i class="fas fa-briefcase"></i>Promos</a>
                        </li>
                        
                        <li>
                            <a href="notification_list.php">
                                <i class="fas fa-bell"></i>Notification</a>
                        </li>
                        <li>
                            <a href="warranty_list.php">
                                <i class="fa fa-file-text"></i>Warranty</a>
                        </li>
                        <li>
                            <a href="reason_list.php">
                                <i class="fas fa-cog"></i>Reason Database</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $fname;?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="admin_profile.php"><?php echo $fname;?> <?php echo $lname;?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $email;?></span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-money-box"></i>Billing</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="admin_index.php?logout='1'">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->
           <!-- MAIN CONTENT-->
           <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                        <form method="post" style="margin-bottom: 5px;">  
                           <input type="submit" name="generate_pdf" class="btn btn-success" value="Generate Reports" />  
                         </form>        
                            <div class="col-md-12">
                            <!-- STATISTIC-->
                            <section class="statistic statistic2">
                                <div class="container">
                                    <div class="row">
                                        <!--/.col-->
                                        <div class="col-6 col-lg-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="clearfix">
                                                    <i class="fa fa-group bg-flat-color-5 p-3 font-2xl mr-3 float-left text-light"></i>
                                                    <div class="h5 text-secondary mb-0 mt-1"><?php echo $count[0]?></div>
                                                    <div class="text-muted text-uppercase font-weight-bold font-xs small">All Handyman</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <!--/.col-->
                                        <div class="col-6 col-lg-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="clearfix">
                                                        <i class="fa fa-check bg-info p-3 font-2xl mr-3 float-left text-light"></i>
                                                        <div class="h5 text-secondary mb-0 mt-1"><?php echo $countActive[0]?></div>
                                                        <div class="text-muted text-uppercase font-weight-bold font-xs small">Active</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/.col-->
                                        
                                        <div class="col-6 col-lg-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="clearfix">
                                                        <i class="fa fa-times bg-secondary p-3 font-2xl mr-3 float-left text-light"></i>
                                                        <div class="h5 text-secondary mb-0 mt-1"><?php echo $countInactive[0]?></div>
                                                        <div class="text-muted text-uppercase font-weight-bold font-xs small">In Active</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!--/.col-->
                                    <div class="col-6 col-lg-3">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="clearfix">
                                                    <i class="fa fa-bell bg-danger p-3 font-2xl mr-3 float-left text-light"></i>
                                                    <div class="h5 text-secondary mb-0 mt-1"><?php echo $countUnverified[0]?></div>
                                                    <div class="text-muted text-uppercase font-weight-bold font-xs small">Unverified</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/.col-->
                                    </div>
                                </div>
                            </section>                               
            <button type="button"  class="btn btn-outline-secondary" onclick="window.location.href='handyman_addform.php'" >
                <i class="fa fa-plus" ></i>&nbsp; Add Handyman</button>
                    <div class="animated fadeIn">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Table</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                            <th>User ID</th> 
                                            <th>Name</th>
                                            <th>Number</th> 
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        while($res = mysqli_fetch_array($result))
                                        {
                                            echo "<tr>";                                            
                                            echo "<td>".$res['user_id']."</td>";
                                            echo "<td>".$res['user_fname']." ".$res['user_lname']."</td>";                                            
                                            echo "<td>".$res['user_number']."</td>";
                                            echo "<td>".$res['user_email']."</td>";
                                            echo "<td>".$res['user_role']."</td>";
                                            echo "<td>
                                                <a class='btn btn-success' href=\"handyman_profile.php?user_id=$res[user_id]\"><i class='fa fa-eye fa-lg'></i></a>
                                            <a href=\"delete.php?id=$res[user_id]\"  class=\"btn btn-danger confirmation\"><i class='fa fa-trash-o fa-lg'></i>
                                                  </a></td>";
                                            echo "</tr>";                                            
                                        }
                                        ?> 
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                     </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
    

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>
</body>
    <script type="text/javascript">
    $('.confirmation').on('click', function () {
        return confirm('Are you sure?');
    });
    </script>
</html>
<!-- end document-->
