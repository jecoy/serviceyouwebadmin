<?php
session_start();
include_once("dbconfig.php");
$id = $_SESSION['user_id'];
if (!isset($_SESSION['user_id'])) {
  header("Location: index.php");
}
if ($_SESSION['user_role'] == 'Subadmin') {
    header("Location: subadmin_index.php");
}
if (isset($_GET['logout'])) 
 {
    $result = mysqli_query($mysqli, "UPDATE `tbl_user` SET `user_status`= 'Offline' where `user_id` = $id");
    session_destroy();
    unset($_SESSION['user_id']);
    header("location: index.php");
 }
 //fetch profile
$query = mysqli_query($mysqli,"Select * from tbl_user where user_id = $id");
while($res = mysqli_fetch_array($query))
{
  $id = $res['user_id'];
  $fname = $res['user_fname'];  
  $lname = $res['user_lname'];
  $address = $res['user_add'];
  $email = $res['user_email'];
  $gender = $res['user_gender'];
  $number = $res['user_number'];
  $password = $res['user_pass'];
  $user_pic = $res['user_pic'];
}

if(isset($_GET['promo_id']))
{
    $promo_id = $_GET['promo_id'];
    $fetch_promo = mysqli_query($mysqli, "SELECT *
                FROM tbl_promos
                where promo_id = $promo_id");

    while($row = mysqli_fetch_array($fetch_promo))
    {
    $promo_id = $row['promo_id'];
    $promo_name = $row['promo_name'];
    $promo_code = $row['promo_code'];
    $promo_type = $row['promo_type'];
    $promo_amount = $row['promo_amount'];
    $promo_limit = $row['promo_limit'];
    $dateCreated = $row['promo_dateCreated'];
    $dateExpire = $row['promo_dateExpire'];
    $promo_status = $row['promo_status'];
    $promo_active = $row['promo_isActive'];
    $promo_price = $row['promo_price'];

    $promo_dateCreated_old = strtotime($dateCreated);
    $promo_dateCreated = date('Y-m-d',$promo_dateCreated_old);
    $promo_dateExpire_old = strtotime($dateExpire);
    $promo_dateExpire = date('Y-m-d',$promo_dateExpire_old);
    }
}

if(isset($_POST['submit']))
{

    $promo_id = mysqli_real_escape_string($mysqli,$_POST['promo_id']);
    $promo_name = mysqli_real_escape_string($mysqli,$_POST['promo_name']);
    $promo_code = mysqli_real_escape_string($mysqli,$_POST['promo_code']);
    $promo_type = mysqli_real_escape_string($mysqli,$_POST['promo_type']);
    $promo_amount = mysqli_real_escape_string($mysqli,$_POST['promo_amount']);
    $promo_limit = mysqli_real_escape_string($mysqli,$_POST['promo_limit']);
    $promo_dateCreated = mysqli_real_escape_string($mysqli,$_POST['promo_dateCreated']);
    $promo_dateExpire = mysqli_real_escape_string($mysqli,$_POST['promo_dateExpire']);
    $promo_status = mysqli_real_escape_string($mysqli,$_POST['promo_status']);
    $promo_price = mysqli_real_escape_string($mysqli,$_POST['promo_price']);
    $promo_active = mysqli_real_escape_string($mysqli,$_POST['promo_active']);

  $result = mysqli_query($mysqli,"UPDATE `tbl_promos` SET `promo_id`='$promo_id',`promo_name`='$promo_name',`promo_code`='$promo_code',`promo_type`='$promo_type',`promo_amount`='$promo_amount',`promo_limit`='$promo_limit',`promo_price`='$promo_price',`promo_dateCreated`='$promo_dateCreated',`promo_dateExpire`='$promo_dateExpire',`promo_status`='$promo_status',`promo_isActive`='$promo_active' WHERE promo_id = $promo_id ");

      echo "<script>alert('Successfully updated');window.location.href='promo_list.php';</script>";
}

?>
<html lang="en">

<head>
     <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Edit Promo</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="SA/vendors/font-awesome/css/font-awesome.min.css">
    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

    <link rel="stylesheet" href="SA/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="SA/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

</head>

<body class="animsition">
    <div class="page-wrapper">
     <!-- MENU SIDEBAR-->
     <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/Service.You.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub">
                            <a class="js-arrow" href="admin_index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            
                        </li>
                       
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-group"></i>Members</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="admin_list.php">
                                    <i class="fas fa-caret-right"></i>Admin</a>
                                </li>
                                <li>
                                    <a href="subadmin_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Admin</a>
                                </li>
                                <li>
                                    <a href="user_list.php">
                                    <i class="fas fa-caret-right"></i>User</a>
                                </li>
                                
                                <li>
                                    <a href="handyman_list.php">
                                    <i class="fas fa-caret-right"></i>Handyman</a>
                                </li>
                            
                            </ul>
                        </li>
                        
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Services</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="service_list.php">
                                    <i class="fas fa-caret-right"></i>Main Services</a>
                                </li>
                                <li>
                                    <a href="subservices_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Services</a>
                                </li>
                                
                            
                            </ul>
                        </li>
                         <li>
                            <a href="reviews_list.php">
                                <i class="fas fa-pencil-square-o"></i>Reviews</a>
                        </li>
                        
                        <li>
                            <a href="reports_list.php">
                                <i class="fas fa-folder-open"></i>Reports</a>
                        </li>
                        
                         <li>
                            <a href="task_list.php">
                                <i class="fas fa-briefcase"></i>Task</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Transaction</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="transaction_completed_list.php">
                                    <i class="fas fa-caret-right"></i>Completed Transaction</a>
                                </li>
                                <li>
                                    <a href="transaction_cancelled_list.php">
                                    <i class="fas fa-caret-right"></i>Cancelled Transaction</a>
                                </li>
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a href="promo_list.php">
                                <i class="fas fa-briefcase"></i>Promos</a>
                        </li>
                        
                        <li>
                            <a href="notification_list.php">
                                <i class="fas fa-bell"></i>Notification</a>
                        </li>
                        <li>
                            <a href="warranty_list.php">
                                <i class="fa fa-file-text"></i>Warranty</a>
                        </li>
                        <li>
                            <a href="reason_list.php">
                                <i class="fas fa-cog"></i>Reason Database</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $fname;?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="admin_profile.php"><?php echo $fname;?> <?php echo $lname;?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $email;?></span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-money-box"></i>Billing</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="admin_index.php?logout='1'">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->           
           <!-- MAIN CONTENT-->       
           <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                            <div class="animsition">
                                <div class="page-wrapper">
                                          <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-header" style="text-align: center;">Promo Information</div>
                                                    <div class="card-body card-block">
                                                        <form action="edit_promo.php"  class="col s12" method="post" id="register" enctype="multipart/form-data" onsubmit="return validation()">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <input class="form-control" type="text" name="promo_id" value="<?php echo $promo_id?>" hidden>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Name</div>
                                                                    <input class="form-control" type="text" name="promo_name" value="<?php echo $promo_name?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Code</div>
                                                                    <input class="form-control" type="text" name="promo_code" value="<?php echo $promo_code?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Type</div>
                                                                    <?php 
                                                                    if($promo_type=='Percentage'){
                                                                       echo "
                                                                       <select id='promo_type' name=\"promo_type\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"Percentage\" selected>Percentage</option>
                                                                          <option value=\"Discount\">Discount</option>
                                                                        </select>";
                                                                    }
                                                                    else{
                                                                        echo "
                                                                       <select id='promo_type' name=\"promo_type\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"Percentage\">Percentage</option>
                                                                          <option value=\"Discount\" selected>Discount</option>
                                                                        </select>";
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Amount</div>
                                                                    <input class="form-control" type="text" name="promo_amount" value="<?php echo $promo_amount?>">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Limit</div>
                                                                    <?php 
                                                                    if($promo_limit=='Bronze'){
                                                                       echo "
                                                                       <select id='promo_limit' name=\"promo_limit\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"Bronze\" selected>Bronze Member</option>
                                                                          <option value=\"Silver\">Silver Member</option>
                                                                          <option value=\"Gold\">Gold Member</option>
                                                                        </select>";
                                                                    }
                                                                    else if($promo_limit=='Silver'){
                                                                        echo "
                                                                       <select id='promo_limit' name=\"promo_limit\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"Bronze\">Bronze Member</option>
                                                                          <option value=\"Silver\" selected>Silver Member</option>
                                                                          <option value=\"Gold\">Gold Member</option>
                                                                        </select>";
                                                                    }
                                                                    else if($promo_limit=='Gold'){
                                                                        echo "
                                                                       <select id='promo_limit' name=\"promo_limit\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"Bronze\">Bronze Member</option>
                                                                          <option value=\"Silver\">Silver Member</option>
                                                                          <option value=\"Gold\" selected>Gold Member</option>
                                                                        </select>";
                                                                    }
                                                                    else{
                                                                        echo "
                                                                       <select id='promo_limit' name=\"promo_limit\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"All\" selected>All</option>
                                                                          <option value=\"Bronze\">Bronze Member</option>
                                                                          <option value=\"Silver\">Silver Member</option>
                                                                          <option value=\"Gold\">Gold Member</option>
                                                                        </select>";
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Price</div>
                                                                    <input class="form-control" type="text" name="promo_price" value="<?php echo $promo_price?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Created</div>
                                                                    <input class="form-control" type="date" name="promo_dateCreated" value="<?php echo $promo_dateCreated?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Expired</div>
                                                                    <input class="form-control" type="date" name="promo_dateExpire" value="<?php echo $promo_dateExpire?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Status</div>
                                                                     <input class="form-control" type="text" name="promo_status" value="<?php echo $promo_status?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Promo Active</div>
                                                                    <?php 
                                                                    if($promo_active=='1'){
                                                                       echo "
                                                                       <select id='promo_active' name=\"promo_active\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"TRUE\" selected>TRUE</option>
                                                                          <option value=\"FALSE\">FALSE</option>
                                                                        </select>";
                                                                    }
                                                                    else{
                                                                        echo "
                                                                       <select id='promo_active' name=\"promo_active\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"TRUE\">TRUE</option>
                                                                          <option value=\"FALSE\" selected>FALSE</option>
                                                                        </select>";
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions form-group">
                                                                <button type="submit" name="submit" id="submitbtn" class="au-btn au-btn--block au-btn--green m-b-20">Submit</button>
                                                            </div>
                                                        </form>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>
    
    
    
    <script src="SA/vendors/jquery/dist/jquery.min.js"></script>
    <script src="SA/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="SA/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="SA/assets/js/main.js"></script>


    <script src="SA/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="SA/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="SA/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="SA/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="SA/vendors/jszip/dist/jszip.min.js"></script>
    <script src="SA/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="SA/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="SA/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="SA/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="SA/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="SA/assets/js/init-scripts/data-table/datatables-init.js"></script>


     <!-- Right Panel -->

    
</body>

</html>
<!-- end document-->
