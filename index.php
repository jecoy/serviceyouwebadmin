<?php
session_start();
include_once("dbconfig.php");
if (isset($_SESSION['user_id'])) 
{
    $id = $_SESSION['user_id'];
  
    $fetch = mysqli_query($mysqli, "SELECT * FROM tbl_user WHERE user_id = $id");
    
    while ($row = mysqli_fetch_array($fetch)) {
      $usertype = $row['user_role'];
        if ($usertype == 'Admin') {
        header('Location: admin_index.php');
      }
    }
  }

if(isset($_POST['submit']))
{
  $email = mysqli_real_escape_string($mysqli,$_POST['email']);
  $password = mysqli_real_escape_string($mysqli,$_POST['password']);
  $result = mysqli_query($mysqli,"Select * from tbl_user where user_email = '$email' and user_pass = '$password'");
  if(mysqli_num_rows($result)==1)
  {
    while($res = mysqli_fetch_array($result))
    {
      if($res['user_role']=="Admin")
      {
        if(!empty($_POST['remember']))
        {
          setcookie("email",$_POST['email'],time()+ (10 * 365 * 24 * 60 * 60));
          setcookie("password",$_POST['password'],time()+ (10 * 365 * 24 * 60 * 60));
        }
        else
        {
          if(isset($_COOKIE['email']))
          {
            setcookie("email","");
          }
          if(isset($_COOKIE['password']))
          {
            setcookie("password","");
          }
        }

        $id = $res['user_id'];
        $result = mysqli_query($mysqli, "UPDATE `tbl_user` SET `user_status`= 'Online' where `user_id` = $id");
        
        $_SESSION['user_role'] = $res['user_role'];
        $_SESSION['user_id'] = $res['user_id'];
        header("Location: admin_index.php");
      }
      else
      {
        if(!empty($_POST['remember']))
        {
          setcookie("email",$_POST['email'],time()+ (10 * 365 * 24 * 60 * 60));
          setcookie("password",$_POST['password'],time()+ (10 * 365 * 24 * 60 * 60));
        }
        else
        {
          if(isset($_COOKIE['email']))
          {
            setcookie("email","");
          }
          if(isset($_COOKIE['password']))
          {
            setcookie("password","");
          }
        }

        $id = $res['user_id'];
        $result = mysqli_query($mysqli, "UPDATE `tbl_user` SET `user_status`= 'Online' where `user_id` = $id");
        
        $_SESSION['user_role'] = $res['user_role'];
        $_SESSION['user_id'] = $res['user_id'];
        header("Location: admin_index.php");
      }
    }
  }
  else
  {
    echo "<script>alert('Invalid username or password')</script>";
  }
}
?>
<html lang="en">
<head>
     <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Admin</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="SA/vendors/font-awesome/css/font-awesome.min.css">
    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

    <link rel="stylesheet" href="SA/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="SA/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="images/icon/Service.You.png" alt="CoolAdmin">
                            </a>
                        </div>
                        <div class="login-form">
                            <form action="index.php" method="post">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input id="email" type="text" class="au-input au-input--full" name="email" value="<?php if(isset($_COOKIE['email'])) { echo $_COOKIE['email']; }?>" required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input id="password" type="password" class="au-input au-input--full" name="password" value="<?php if(isset($_COOKIE['password'])) { echo $_COOKIE['password']; }?>" required>
                                </div>
                                <div class="login-checkbox">
                                <label>
                                <input type="checkbox" id="check" name="remember">
                                Remember Me
                                </label>
                                <label>
                                <a href="forgot_pass.php">Forgotten Password?</a>
                                </label>
                                </div>
                                 <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="submit">Sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->