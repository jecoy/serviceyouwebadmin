<?php
session_start();
include_once("dbconfig.php");
$id = $_SESSION['user_id'];
if (!isset($_SESSION['user_id'])) {
  header("Location: index.php");
}
if ($_SESSION['user_role'] == 'Subadmin') {
    header("Location: subadmin_index.php");
}

if (isset($_GET['logout'])) 
 {
    $result = mysqli_query($mysqli, "UPDATE `tbl_user` SET `user_status`= 'Offline' where `user_id` = $id");
    session_destroy();
    unset($_SESSION['user_id']);
    header("location: index.php");
 }
 //fetch profile
$query = mysqli_query($mysqli,"Select * from tbl_user where user_id = $id");
$query2 = mysqli_query($mysqli,"Select * from tbl_service");
while($res = mysqli_fetch_array($query))
{
  $id = $res['user_id'];
  $fname = $res['user_fname'];  
  $lname = $res['user_lname'];
  $address = $res['user_add'];
  $email = $res['user_email'];
  $gender = $res['user_gender'];
  $number = $res['user_number'];
  $password = $res['user_pass'];
  $user_pic = $res['user_pic'];
}

if(isset($_GET['subservice_id']))
{
    $subservice_id = $_GET['subservice_id'];
    $fetch_service = mysqli_query($mysqli, "SELECT *
                FROM tbl_subservice
                where subservice_id = $subservice_id");

    while($row = mysqli_fetch_array($fetch_service))
    {
    $subservice_id = $row['subservice_id'];
    $service_id = $row['service_id'];
    $subservice = $row['subservice'];
    $old_icon = $row['subservice_icon'];
    $subservice_rate = $row['subservice_rate'];
    $subservice_status = $row['subservice_status'];
    $subservice_isActive = $row['subservice_isActive'];
    }
}

if(isset($_POST['submit']))
{
  $subservice_id = mysqli_real_escape_string($mysqli,$_POST['subservice_id']);
  $service_id = mysqli_real_escape_string($mysqli,$_POST['service_id']);
  $subservice = mysqli_real_escape_string($mysqli,$_POST['subservice']);
  $subservice_status = mysqli_real_escape_string($mysqli,$_POST['subservice_status']);
  $subservice_isActive = mysqli_real_escape_string($mysqli,$_POST['subservice_isActive']);
  $subservice_rate = mysqli_real_escape_string($mysqli,$_POST['subservice_rate']);
  $old_icon = mysqli_real_escape_string($mysqli,$_POST['old_icon']);

  if(isset($_FILES['subservicepic']['name'])||$_FILES['subservicepic']['name']!="")
    {
      $filename = $_FILES['subservicepic']['name'];
      $filetmpname = $_FILES['subservicepic']['tmp_name'];
      $folder = "images/subservicepics/".basename($_FILES['subservicepic']['name']);
      move_uploaded_file($filetmpname,$folder);
     
    }
    else
    {
      $filename = $old_icon;
    }

      $result = mysqli_query($mysqli,"UPDATE `tbl_subservice` SET `service_id`='$service_id', `subservice`='$subservice',`subservice_icon`='$filename',`subservice_rate`='$subservice_rate',`subservice_status`='$subservice_status',`subservice_isActive`='$subservice_isActive' where subservice_id = $subservice_id");

      echo "<script>alert('Successfully updated!');window.location.href='subservices_list.php';</script>";
}
?>
<html lang="en">

<head>
   <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Edit Subservices</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="SA/vendors/font-awesome/css/font-awesome.min.css">
    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

    <link rel="stylesheet" href="SA/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="SA/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
<style type="text/css">
    #profileDisplay { display: block; height: 210px; width: 30%; margin: 0px auto; border-radius: 50%; }
.img-placeholder {
  width: 30%;
  color: white;
  height: 60%;
  background: black;
  opacity: .7;
  height: 210px;
  border-radius:50%;
  z-index: 2;
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  display: none;
}
.img-placeholder h4 {
  margin-top: 40%;
  color: white;
}
.img-div:hover .img-placeholder {
  display: block;
  cursor: pointer;
  background-color: white;
}
</style>
</head>
<body class="animsition">
    <div class="page-wrapper">
     <!-- MENU SIDEBAR-->
     <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/Service.You.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a class="js-arrow" href="admin_index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            
                        </li>
                       
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-group"></i>Members</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="admin_list.php">
                                    <i class="fas fa-caret-right"></i>Admin</a>
                                </li>
                                <li>
                                    <a href="subadmin_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Admin</a>
                                </li>
                                <li>
                                    <a href="user_list.php">
                                    <i class="fas fa-caret-right"></i>User</a>
                                </li>
                                
                                <li>
                                    <a href="handyman_list.php">
                                    <i class="fas fa-caret-right"></i>Handyman</a>
                                </li>
                            
                            </ul>
                        </li>
                        
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Services</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="service_list.php">
                                    <i class="fas fa-caret-right"></i>Main Services</a>
                                </li>
                                <li>
                                    <a href="subservices_list.php">
                                    <i class="fas fa-caret-right"></i>Sub-Services</a>
                                </li>
                                
                            
                            </ul>
                        </li>
                         <li>
                            <a href="reviews_list.php">
                                <i class="fas fa-pencil-square-o"></i>Reviews</a>
                        </li>
                        
                        <li>
                            <a href="reports_list.php">
                                <i class="fas fa-folder-open"></i>Reports</a>
                        </li>
                        
                         <li>
                            <a href="task_list.php">
                                <i class="fas fa-briefcase"></i>Task</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-wrench"></i>Transaction</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="transaction_completed_list.php">
                                    <i class="fas fa-caret-right"></i>Completed Transaction</a>
                                </li>
                                <li>
                                    <a href="transaction_cancelled_list.php">
                                    <i class="fas fa-caret-right"></i>Cancelled Transaction</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="promo_list.php">
                                <i class="fas fa-briefcase"></i>Promos</a>
                        </li>
                        
                        <li>
                            <a href="notification_list.php">
                                <i class="fas fa-bell"></i>Notification</a>
                        </li>
                        <li>
                            <a href="warranty_list.php">
                                <i class="fa fa-file-text"></i>Warranty</a>
                        </li>
                        <li>
                            <a href="reason_list.php">
                                <i class="fas fa-cog"></i>Reason Database</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $fname;?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="images/profilepic/<?php echo $user_pic;?>" alt="<?php echo $fname;?>" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="admin_profile.php"><?php echo $fname;?> <?php echo $lname;?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $email;?></span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-money-box"></i>Billing</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="admin_index.php?logout='1'">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->
           <!-- MAIN CONTENT-->
           <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                            <div class="animsition">
                                <div class="page-wrapper">
                                          <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-header">Edit Service Form</div>
                                                    <div class="card-body card-block">
                                                        <form action="edit_subservice.php"  class="col s12" method="post" id="register" enctype="multipart/form-data" onsubmit="return validation()">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <input type="text" id="subservice_id" name="subservice_id" name="subservice_id" class="form-control" value="<?php echo $subservice_id?>" hidden>
                                                                    <input type="text" id="old_icon" name="old_icon" name="old_icon" class="form-control" value="<?php echo $old_icon?>" hidden>
                                                                </div>
                                                            </div>
                                                           <div class="form-group text-center" style="position: relative;" >
                                                                    <span class="img-div">
                                                                      <div class="text-center img-placeholder"  onClick="triggerClick()">
                                                                      </div>
                                                                      <img src="images/subservicepics/<?php echo $old_icon?>" onClick="triggerClick()" id="profileDisplay" style="border-radius: 0%; width: 200px; height: 200px;">
                                                                    </span>
                                                                    <input id="subservicepic" name="subservicepic" type="file" onChange="displayImage(this)" class="form-control" style="display: none;">
                                                                    <label>Subservice Icon</label>
                                                                </div>     
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Parent Service</div>
                                                                    <select id='service_id' name="service_id" class="form-control" required>
                                                                            <?php
                                                                              while($res = mysqli_fetch_array($query2))
                                                                              {
                                                                                if($service_id == $res['service_id']){
                                                                                echo '<option value="'.$res['service_id'].'" selected>' . $res['service'] . "</option>";
                                                                                }
                                                                                else{
                                                                                    echo '<option value="'.$res['service_id'].'">' . $res['service'] . "</option>";
                                                                                }
                                                                              }
                                                                              ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Subservice</div>
                                                                    <input type="text" id="subservice" name="subservice" name="subservice" class="form-control" value="<?php echo $subservice?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Subservice Rate</div>
                                                                    <input type="text" id="subservice_rate" name="subservice_rate" name="subservice_rate" class="form-control" value="<?php echo $subservice_rate?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Subservice Status</div>
                                                                    <input type="text" id="subservice_status" name="subservice_status" name="subservice_status" class="form-control" value="<?php echo $subservice_status?>" required>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">Subservice Active</div>
                                                                    <?php 
                                                                    if($subservice_isActive=='1'){
                                                                       echo "
                                                                       <select id='subservice_isActive' name=\"subservice_isActive\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"TRUE\" selected>TRUE</option>
                                                                          <option value=\"FALSE\">FALSE</option>
                                                                        </select>";
                                                                    }
                                                                    else{
                                                                        echo "
                                                                       <select id='subservice_isActive' name=\"subservice_isActive\" 
                                                                       class=\"form-control\" required>
                                                                          <option value=\"TRUE\">TRUE</option>
                                                                          <option value=\"FALSE\" selected>FALSE</option>
                                                                        </select>";
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions form-group">
                                                                <button type="submit" name="submit" id="submitbtn" class="au-btn au-btn--block au-btn--green m-b-20">Submit</button>
                                                            </div>
                                                        </form>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js"></script>

    <!-- Main JS-->
    <script src="js/main.js"></script>
     <!-- Right Panel -->
     <script type="text/javascript">
    function triggerClick(e) {
  document.querySelector('#subservicepic').click();
    }
    function displayImage(e) {
      if (e.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e){
          document.querySelector('#profileDisplay').setAttribute('src', e.target.result);
        }
        reader.readAsDataURL(e.files[0]);
      }
    }
    </script>
</body>

</html>
<!-- end document-->
