<?php

header("Content-Type: application/json; charset=UTF-8");
require_once 'dbconfig.php';
$key = $_POST['key'];
if($key == "insertTransactionInfo"){

	$customer_id = $_POST['customer_id'];
	$service_id = $_POST['service_id'];
	$subservice_id = $_POST['subservice_id'];
	$task_date = $_POST['task_date'];
	$task_location = $_POST['task_location'];
	$task_instruction = $_POST['task_instruction'];
	$promo_id = $_POST['promo_id'];
	$latitude = $_POST['latitude'];
	$longitude = $_POST['longitude'];
	$longitude = $_POST['longitude'];
	$task_type = $_POST['task_type'];
	$today = date("Y-m-d");
	
	$insertTransactionInfoQuery = mysqli_query($mysqli,"INSERT INTO `tbl_transactioninfo` (`service_id`, `subservice_id`,`task_type`,`task_date`, `task_location`,`latitude`,`longitude`,`task_instruction`, `task_status`, `customer_id`, `ti_isActive`) 
		VALUES ('$service_id','$subservice_id','$task_type','$task_date','$task_location','$latitude','$longitude','$task_instruction','Pending','$customer_id',TRUE)");

	$ti_id = mysqli_insert_id($mysqli);
	if($promo_id=='0')
	{
	$insertTransactionQuery = mysqli_query($mysqli,"INSERT INTO `tbl_transaction`(`ti_id`, `transaction_date`) VALUES ('$ti_id','$today')");
	}
	else
	{		
	$insertTransactionQuery = mysqli_query($mysqli,"INSERT INTO `tbl_transaction`(`ti_id`, `transaction_date`,`promo_id`) VALUES ('$ti_id','$today','$promo_id')");
	}

	if($insertTransactionInfoQuery){
		$result["ti_id"] = $ti_id;
		$result["value"] = "1";
        $result["message"] = "Transaction Pending";
	}

	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="fetchNotifications"){
  $user_id = $_POST['user_id'];
  $selectNotifications = mysqli_query($mysqli,"SELECT * FROM `tbl_notif` WHERE user_id = '$user_id'");
  $result = array();
  if($selectNotifications){
    while($res = mysqli_fetch_array($selectNotifications))
          {
            array_push($result, 
            array(
                'notif_id'=>$res['notif_id'], 
                'user_id'=>$res['user_id'],
                'notif_subj' => $res['notif_subj'],
                'notif_date'=>$res['notif_date'],
                'notif_content'=>$res['notif_content'], 
                'notif_isSeen' => $res['notif_isSeen'],
                'notif_isActive '=>$res['notif_isActive']
              )
            );
          }
  }

  else{
    $response["value"] = "0";
        $response["message"] = "Error at first query ! ".mysqli_error($mysqli);
        echo json_encode($response);
        mysqli_close($mysqli);
  }


  echo json_encode($result);
  mysqli_close($mysqli);
}

else if($key=="checkTransactionHandyman"){
	$ti_id = $_POST['ti_id'];
	$customer_id = $_POST['customer_id'];

	$checkTransactionHandymanQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` WHERE `ti_id`='$ti_id' && `customer_id` = '$customer_id' && `task_status` = 'Accepted' || `task_status` = 'Reserved'");

	if($checkTransactionHandymanQuery){	
		while ($row = mysqli_fetch_assoc($checkTransactionHandymanQuery)) {
			$result["ti_id"] = $row['ti_id'];
			$result["service_id"] = $row['service_id'];
			$result["subservice_id"] = $row['subservice_id'];
			$result["customer_id"] = $row['customer_id'];
			$result["handyman_id"] = $row['handyman_id'];
			$result["task_type"] = $row['task_type'];
			$handyman_id = $row['handyman_id'];

			$result["value"] = "1";
	        $result["message"] = "Transaction Accepted";
		}

			$selectConvo = mysqli_query($mysqli,"SELECT * FROM `tbl_conversation` WHERE `handyman_id`='$handyman_id' && `customer_id` = '$customer_id'");
			
			if($selectConvo){
				while ($row = mysqli_fetch_assoc($selectConvo)) {
					$conversation_id = $row['conversation_id'];
					$result["conversation_id"] = $conversation_id;
				}
			}

			else{
				$response["value"] ="0";
				$response["message"] ="Error at second query";
				json_encode($response);
				mysqli_close($mysqli);
			}
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="fetchHandymanProfle"){
	$handyman_id = $_POST['user_id'];
	$ti_id = $_POST['value'];
	$response = array();

	$fetchHandymanProfileQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_user` 
		INNER JOIN `tbl_hmprofile` on tbl_user.user_id = tbl_hmprofile.user_id
		INNER JOIN `tbl_handymanservices` on tbl_hmprofile.user_id = tbl_handymanservices.user_id
		INNER JOIN `tbl_subservice` on tbl_handymanservices.subservice_id = tbl_subservice.subservice_id
		INNER JOIN `tbl_transactioninfo` on tbl_subservice.subservice_id = tbl_transactioninfo.subservice_id
		 WHERE tbl_user.user_id = $handyman_id && tbl_transactioninfo.ti_id = '$ti_id'");
	$count_service = mysqli_fetch_array(mysqli_query($mysqli, "SELECT AVG(ratings) FROM tbl_ratesandreview 
																				WHERE user_id = '$handyman_id'"));
		if($fetchHandymanProfileQuery){
			while ($row = mysqli_fetch_assoc($fetchHandymanProfileQuery)) {
				$result["user_id"] = $row['user_id'];
				$result["user_fname"] = $row['user_fname'];
				$result["user_lname"] = $row['user_lname'];
				$result["user_add"] = $row['user_add'];
				$result["user_number"] = $row['user_number'];
				$result["user_email"] = $row['user_email'];
				$result["user_pic"] = "http://192.168.43.65:8085/ServiceYou/images/profilepic/" . $row['user_pic'];
				$result["user_description"] = $row['user_description'];
				$result["user_workDescription"] = $row['user_workDescription'];
				$result["service_quickpitch"] = $row['service_quickpitch'];
				$result["value"] = "1";
				$result["message"] = "Success retrieving handyman info.";
			}
				$result["rating"] = $count_service[0];
		}
		else{
			      $response["value"] = "0";
	              $response["message"] = "Error at updating query ! ".mysqli_error($mysqli);
	              echo json_encode($response);
	              mysqli_close($mysqli);
		}


	echo json_encode($result);
	mysqli_close($mysqli);
}

else if($key=="checkAvailablePromo"){

	$today = date("Y-m-d");
	$user_id = $_POST['user_id'];

	$checkUserQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_user` INNER JOIN `tbl_customerprofile` ON tbl_user.user_id = tbl_customerprofile.user_id WHERE tbl_user.user_id='$user_id'");

	$response = array();

	if($checkUserQuery){
		while ($row = mysqli_fetch_assoc($checkUserQuery)) {
			$user_badge = $row['user_badge'];
			$_userID = 	$row['user_id'];
		}

		if($user_badge=='Bronze'){
			$checkPromoQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_promos` where (`promo_limit` = '$user_badge' OR `promo_limit`='All') AND (`promo_isActive` = TRUE AND `promo_status` = 'Published')");
		}
		else if($user_badge=='Silver'){
			$checkPromoQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_promos` where (`promo_isActive` = TRUE AND `promo_status` = 'Published') AND ((`promo_limit`='$user_badge' OR `promo_limit`='All') OR `promo_limit`='Bronze') ");
		}
		else if($user_badge=='Gold'){
			$checkPromoQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_promos` where (`promo_isActive` = TRUE AND `promo_status` = 'Published') AND
			 ((`promo_limit`='$user_badge' OR `promo_limit`='All') OR (`promo_limit`='Bronze' OR `promo_limit`='Silver'))");
		}

		else{
			$response["value"] = "0";
		    $response["message"] = "Error at second query ! ".mysqli_error($mysqli);
		    echo json_encode($response);
		    mysqli_close($mysqli);
		}

			if($checkPromoQuery){
				while ($row = mysqli_fetch_assoc($checkPromoQuery)) {
					$promo_id = $row['promo_id'];

					$checkIfUsedQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transaction` INNER JOIN `tbl_transactioninfo` on tbl_transaction.ti_id = tbl_transactioninfo.ti_id WHERE tbl_transactioninfo.customer_id ='$_userID' AND tbl_transaction.promo_id = '$promo_id'");

					$counter = mysqli_num_rows($checkIfUsedQuery);
					if($counter<1){
						if($today < $row['promo_dateExpire']){
							array_push($response, 
					        array(
					            'promo_id'=>$row['promo_id'], 
					            'promo_code' => $row['promo_code'],
					            'promo_type'=>$row['promo_type'], 
					            'promo_amount'=>$row['promo_amount'],
					            'promo_price'=>$row['promo_price']
					        	)
					        );
						}
					}
				}
			}

			else{
				$result["value"] = "0";
			    $result["message"] = "Error at second query ! ".mysqli_error($mysqli);
			    echo json_encode($result);
			    mysqli_close($mysqli);
			}

	}
	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
	mysqli_close($mysqli);
}

else if($key=="checkAvaialableTransaction"){
	$handyman_id = $_POST['handyman_id'];

	$checkAvaialableTransactionQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` 
	INNER JOIN `tbl_subservice` ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id
	INNER JOIN `tbl_handymanservices` ON tbl_subservice.subservice_id = tbl_handymanservices.subservice_id
	INNER JOIN `tbl_user` ON tbl_handymanservices.user_id = tbl_user.user_id WHERE tbl_handymanservices.user_id = '$handyman_id' AND tbl_transactioninfo.task_status = 'Pending'");

	if($checkAvaialableTransactionQuery){
		while ($row = mysqli_fetch_assoc($checkAvaialableTransactionQuery)) {
			if($row['user_points']>=$row['subservice_rate']*.10){
				$result["ti_id"] = $row['ti_id'];
				$result["service_id"] = $row['service_id'];
				$result["subservice_id"] = $row['subservice_id'];
				$result["task_date"] = $row['task_date'];
				$result["task_location"] = $row['task_location'];
				$result["latitude"] = $row['latitude'];
				$result["longitude"] = $row['longitude'];
				$result["task_instruction"] = $row['task_instruction'];
				$result["task_status"] = $row['task_status'];
				$result["customer_id"] = $row['customer_id'];
				$result["subservice"] = $row['subservice'];

				$result["value"] = "1";
		        $result["message"] = "Success Fetching";
			}
			
		}
	}
	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($result);
	mysqli_close($mysqli);
}

else if($key=="handymanAcceptTransaction"){
	$ti_id = $_POST['ti_id'];
	$handyman_id = $_POST['handyman_id'];
	$customer_id = $_POST['customer_id'];
	$today = date("Y-m-d H:i:s"); 

	$selectTransaction =  mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` WHERE `ti_id` = '$ti_id'");
	if($selectTransaction){
		while ($row = mysqli_fetch_assoc($selectTransaction)) {
			$task_type = $row['task_type'];
			$result["task_type"] = $row['task_type'];
		}

		if($task_type=='OnTheSpot'){
		$updateTransactionInfoQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Accepted',`handyman_id`='$handyman_id' WHERE `ti_id` = '$ti_id'");
		}

		else{
		$updateTransactionInfoQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Reserved',`handyman_id`='$handyman_id' WHERE `ti_id` = '$ti_id'");
		}
	}

	$selectConvo =  mysqli_query($mysqli,"SELECT * FROM `tbl_conversation` WHERE customer_id='$customer_id' AND handyman_id='$handyman_id'");
	if(mysqli_num_rows($selectConvo)>0){
		while ($row = mysqli_fetch_assoc($selectConvo)) {
			$result["conversation_id"] = $row['conversation_id'];
		}
	}
	else{
		$insertConvo =  mysqli_query($mysqli,"INSERT INTO `tbl_conversation`(`handyman_id`, `customer_id`, `conversation_date`, `conversation_status`, `conversation_isActive`) VALUES ('$handyman_id','$customer_id','$today','Published','TRUE')");

		$result["conversation_id"] = mysqli_insert_id($mysqli);
	}

	$selectCustomer =  mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` INNER JOIN `tbl_user` ON tbl_transactioninfo.customer_id = tbl_user.user_id WHERE tbl_user.user_id = '$customer_id'");

	if($updateTransactionInfoQuery&&$selectCustomer){
		while ($row = mysqli_fetch_assoc($selectCustomer)) {
			$result["user_fname"] = $row['user_fname'];
			$result["user_lname"] = $row['user_lname'];
			$result["user_email"] = $row['user_email']; 
			$result["task_location"] = $row['task_location'];
		}
		$result["value"] = "1";	
	    $result["message"] = "Transaction Updated";
	}
	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="checkReasons"){
	$checkReasonsQuery = mysqli_query($mysqli,"SELECT * FROM tbl_reasons WHERE reason_status = 'Published' && reason_isActive = TRUE");
	$response = array();

	if($checkReasonsQuery){
		while ($row = mysqli_fetch_assoc($checkReasonsQuery)) {
			array_push($response, 
				array(
				'reasons_id'=>$row['reasons_id'], 
				'reason' => $row['reason']
				)
			);
		}
	}
	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="cancelHandyman"){
	$ti_id = $_POST['ti_id'];
	$handyman_id = $_POST['handyman_id'];
	$customer_id = $_POST['customer_id'];
	$reason_id = $_POST['reason_id'];
	$today = date("Y-m-d");

	$updateTransactionInfoQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Cancelled' WHERE `ti_id` = '$ti_id'");
	if($updateTransactionInfoQuery)
	{
		$selectTransactionID = mysqli_query($mysqli,"SELECT * FROM `tbl_transaction` WHERE `ti_id` = '$ti_id'");

		if($selectTransactionID)
		{
			while ($row = mysqli_fetch_assoc($selectTransactionID)) 
			{
				$transaction_id = $row['transaction_id'];
			}

			$insertCancelledTransactionQuery = mysqli_query($mysqli,"INSERT INTO `tbl_cancelledtransac` 
				(`transaction_id`, `ct_date`, `reasons_id`,`ct_status`,`ct_isActive`,`ct_by`) VALUES 
				('$transaction_id','$today','$reason_id','Published',TRUE,'$customer_id')");

			if($insertCancelledTransactionQuery)
			{
				$result["value"] = "1";
		    	$result["message"] = "Success !";
			}
			else
			{
				$result["value"] = "0";
			    $result["message"] = "Error at third query ! ".mysqli_error($mysqli);
			    echo json_encode($result);
			    mysqli_close($mysqli);
			}
		}
		else
		{
			$result["value"] = "0";
		    $result["message"] = "Error at second query ! ".mysqli_error($mysqli);
		    echo json_encode($result);
		    mysqli_close($mysqli);
		}
	}

	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="deletePendingTransaction"){
	$ti_id = $_POST['ti_id'];
	$deletePendingTransactionQuery = mysqli_query($mysqli,"DELETE FROM `tbl_transaction` WHERE `ti_id` = '$ti_id'");
	if($deletePendingTransactionQuery)
	{
		$deletePendingTransactionInfoQuery = mysqli_query($mysqli,"DELETE FROM `tbl_transactioninfo` WHERE `ti_id` = '$ti_id'");	
		if($deletePendingTransactionInfoQuery){
			$response["value"] = "1";
			$response["message"] = "Success";
		}
		else
		{
			$result["value"] = "0";
			$result["message"] = "Error at second query ! ".mysqli_error($mysqli);
			echo json_encode($result);
			mysqli_close($mysqli);
		}	
	}
	else
	{
		$result["value"] = "0";
		$result["message"] = "Error at first query ! ".mysqli_error($mysqli);
		echo json_encode($result);
		mysqli_close($mysqli);
	}
	echo json_encode($response);
	mysqli_close($mysqli);
}

else if($key=="checkTransactionAvailability"){
	$ti_id = $_POST['ti_id'];
	$user_id = $_POST['user_id'];
	$checkTransactionQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` WHERE `ti_id`='$ti_id'");
	if($checkTransactionQuery)
	{
		if(mysqli_num_rows($checkTransactionQuery)>0)
		{
			while ($row = mysqli_fetch_assoc($checkTransactionQuery)) {
				$status = $row['task_status'];
				$handyman_id = $row['handyman_id'];
			}

			if($status=='Accepted'&&$handyman_id!=$user_id)
			{
			$response["value"] = "2";
			$response["message"] = "Accepted";
			}

			else if($status=='Pending')
			{
			$response["value"] = "1";
			$response["message"] = "Pending";
			}

		}
		else if(mysqli_num_rows($checkTransactionQuery)==0)
		{
			$response["value"] = "3";
			$response["message"] = "Cancelled";
		}	

		else
		{
			$result["value"] = "0";
			$result["message"] = "Error at second query ! ".mysqli_error($mysqli);
			echo json_encode($result);
			mysqli_close($mysqli);
		}

	}

	else
	{
		$result["value"] = "0";
		$result["message"] = "Error at first query ! ".mysqli_error($mysqli);
		echo json_encode($result);
		mysqli_close($mysqli);
	}

	echo json_encode($response);
	mysqli_close($mysqli);
}

else if($key=="fetchTransactions"){
	$user_role = $_POST['user_role'];
	$user_id = $_POST['user_id'];
	$filter = $_POST['filter'];
	$response = array();

	if($user_role=="Handyman"){
		$filterRole = 'tbl_transactioninfo.handyman_id';
	}
	else{
		$filterRole = 'tbl_transactioninfo.customer_id';
	}

	if($filter == ""){
	$fetchTransactionQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` 
	INNER JOIN `tbl_subservice` ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id where $filterRole = '$user_id' AND (tbl_transactioninfo.task_status != 'Completed' AND tbl_transactioninfo.task_status != 'Rescheduled' AND tbl_transactioninfo.task_status != 'Cancelled')");
	}
	else{
	$fetchTransactionQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` 
	INNER JOIN `tbl_subservice` ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id where $filterRole = '$user_id' AND tbl_transactioninfo.task_status = '$filter'");
	}
	if($fetchTransactionQuery){
		while ($row = mysqli_fetch_assoc($fetchTransactionQuery)) {
			array_push($response, 
				array(
				'customer_id'=>$row['customer_id'], 
				'handyman_id' => $row['handyman_id'],
				'ti_id'=>$row['ti_id'], 
				'service_id' => $row['service_id'],
				'subservice_id'=>$row['subservice_id'], 
				'latitude' => $row['latitude'],
				'longitude'=>$row['longitude'], 
				'task_date' => $row['task_date'],
				'task_location'=>$row['task_location'], 
				'task_instruction' => $row['task_instruction'],
				'task_status'=>$row['task_status'], 
				'subservice'=>$row['subservice'], 
				'task_type' => $row['task_type'],
				'ti_isActive'=>$row['ti_isActive'], 
				)
			);
		}		
	}

	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="fetchTransactionsUpdate"){
	$user_role = $_POST['user_role'];
	$user_id = $_POST['user_id'];
	$response = array();

	if($user_role=="Handyman"){
		$filterRole = 'tbl_transactioninfo.handyman_id';
	}
	else{
		$filterRole = 'tbl_transactioninfo.customer_id';
	}

	$fetchTransactionQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transaction` 
	INNER JOIN `tbl_transactioninfo` ON tbl_transaction.ti_id = tbl_transactioninfo.ti_id
	INNER JOIN `tbl_subservice` ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id where $filterRole = '$user_id' AND tbl_transactioninfo.task_status = 'Completed'");
	
	if($fetchTransactionQuery){
		while ($row = mysqli_fetch_assoc($fetchTransactionQuery)) {
			array_push($response, 
				array(
				'customer_id'=>$row['customer_id'], 
				'handyman_id' =>$row['handyman_id'],
				'ti_id'=>$row['ti_id'], 
				'transaction_id' =>$row['transaction_id'],
				'transaction_date'=>$row['transaction_date'], 
				'transaction_earnings' =>$row['transaction_earnings']
				)
			);
		}		
	}

	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="fetchWarrantyList"){
	$user_role = $_POST['user_role'];
	$user_id = $_POST['user_id'];
	$today = date('Y-m-d');
	$response = array();

	if($user_role=="Handyman"){
		$filterRole = 'tbl_transactioninfo.handyman_id';
	}
	else{
		$filterRole = 'tbl_transactioninfo.customer_id';
	}

	$fetchTransactionQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_warranty`
	INNER JOIN `tbl_transaction` ON tbl_warranty.transaction_id = tbl_transaction.transaction_id 
	INNER JOIN `tbl_transactioninfo` ON tbl_transaction.ti_id = tbl_transactioninfo.ti_id
	INNER JOIN `tbl_subservice` ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id where $filterRole = '$user_id'");
	
	if($fetchTransactionQuery){
		while ($row = mysqli_fetch_assoc($fetchTransactionQuery)) {
			if($today < $row['warranty_expiration']){
			array_push($response, 
				array(
				'customer_id'=>$row['customer_id'], 
				'handyman_id' =>$row['handyman_id'],
				'ti_id'=>$row['ti_id'], 
				'transaction_id' =>$row['transaction_id'],
				'warranty_id' =>$row['warranty_id'],
				'warranty_status' =>$row['warranty_status'],
				'warranty_expiration' =>$row['warranty_expiration'])
			);
			}
		}		
	}

	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="requestWarranty"){
	$warranty_id = $_POST['warranty_id'];
	$instruction = $_POST['instruction'];
	$today = date('Y-m-d H:i:S');

	$requestWarranty = mysqli_query($mysqli,"INSERT INTO `tbl_warrantyinfo`(`warranty_id`,`warranty_date`,`warranty_instruction`,`warrantyInfo_status`) VALUES ('$warranty_id','$today','$instruction','Published')");

	if($requestWarranty){
	$fetchWarranty = mysqli_query($mysqli,"SELECT * FROM `tbl_warranty` INNER JOIN `tbl_transaction` 
	ON tbl_warranty.transaction_id = tbl_transaction.transaction_id INNER JOIN `tbl_transactioninfo`
	ON tbl_transaction.ti_id = tbl_transactioninfo.ti_id WHERE tbl_warranty.warranty_id = '$warranty_id'");
		if($fetchWarranty){
			while ($row = mysqli_fetch_assoc($fetchWarranty)) {
			$user_id = $row['handyman_id'];
			}
			$updateWarranty = mysqli_query($mysqli,"UPDATE `tbl_warranty` SET `warranty_status`= 'Requested' WHERE `warranty_id`='$warranty_id'");

			if($updateWarranty){
				$response["value"] = "1";			
				$response["message"] = "Success";	
			}
			else{
				$result["value"] = "0";
			    $result["message"] = "Error at third query ! ".mysqli_error($mysqli);
			    echo json_encode($result);
			    mysqli_close($mysqli);	
			}
		}
		else{
				$result["value"] = "0";
			    $result["message"] = "Error at third query ! ".mysqli_error($mysqli);
			    echo json_encode($result);
			    mysqli_close($mysqli);	
		}
		
			
	}
	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}
	echo json_encode($response);
    mysqli_close($mysqli);	
}

else if($key=="fetchRateAndReview"){
	$user_role = $_POST['user_role'];
	$user_id = $_POST['user_id'];
	$response = array();


	$fetchUserQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_ratesandreview`
	INNER JOIN `tbl_user` ON tbl_ratesandreview.user_id = tbl_user.user_id where tbl_ratesandreview.user_id = '$user_id'");
	
	if($fetchUserQuery){
		while ($row = mysqli_fetch_assoc($fetchUserQuery)) {

			$rater_id =  $row['rater_id'];
			$rating_id = $row['rating_id'];
			$fetchRaterQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_ratesandreview`
			INNER JOIN `tbl_user` ON tbl_ratesandreview.rater_id = tbl_user.user_id where tbl_ratesandreview.rater_id = '$rater_id' AND tbl_ratesandreview.rating_id = '$rating_id'");

				while ($row = mysqli_fetch_assoc($fetchRaterQuery)) {
				array_push($response, 
						array(
						'rating_id'=>$row['rating_id'], 
						'transaction_id' =>$row['transaction_id'],
						'rater_email'=>$row['user_email'], 
						'rating_date' =>$row['rating_date'],
						'reviews' =>$row['reviews'],
						'ratings' =>$row['ratings'],
						'user_pic' => "http://192.168.43.65:8085/ServiceYou/images/profilepic/" . $row['user_pic']
						)
					);
				}
		}		
	}

	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="fetchApplicationReport"){
	$user_role = $_POST['user_role'];
	$user_id = $_POST['user_id'];
	$response = array();

	$fetchUserQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_reports`
	INNER JOIN `tbl_user` ON tbl_reports.user_id = tbl_user.user_id where tbl_reports.user_id = '$user_id'");
	
	if($fetchUserQuery){
		while ($row = mysqli_fetch_assoc($fetchUserQuery)) {
				array_push($response, 
						array(
						'reports_id'=>$row['reports_id'], 
						'user_id' =>$row['user_id'],
						'user_email'=>$row['user_email'], 
						'report_date' =>$row['report_date'],
						'report' =>$row['report'],
						'user_pic' => "http://192.168.43.65:8085/ServiceYou/images/profilepic/" . $row['user_pic']
						)
					);
		}		
	}

	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="fetchConversations"){
	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];
	$response = array();

	if($user_role=='Handyman'){
	$fetchPartnerQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_conversation`
	INNER JOIN `tbl_user` ON tbl_conversation.handyman_id = tbl_user.user_id
	 where tbl_conversation.handyman_id = '$user_id'");
		if($fetchPartnerQuery){
			while ($row = mysqli_fetch_assoc($fetchPartnerQuery)) {

				$customer_id = $row['customer_id'];
				$fetchPartnerInfoQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_user` where user_id = '$customer_id'");

				while ($res =mysqli_fetch_assoc($fetchPartnerInfoQuery)) {
						$user_email = $res['user_email'];
						$user_pic = "http://192.168.43.65:8085/ServiceYou/images/profilepic/" . $res['user_pic'];
				}

				array_push($response, 
						array(
						'conversation_id'=>$row['conversation_id'], 
						'conversation_date' =>$row['conversation_date'],
						'conversation_status'=>$row['conversation_status'],
						'conversation_partner' =>$user_email,
						'conversation_picture' => $user_pic
						)
					);
		}
		}
	}

	else if($user_role=='Customer')
	{
	$fetchPartnerQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_conversation`
	INNER JOIN `tbl_user` ON tbl_conversation.customer_id = tbl_user.user_id
	 where tbl_conversation.customer_id = '$user_id'");
		if($fetchPartnerQuery&&$user_role=='Customer'){
			while ($row = mysqli_fetch_assoc($fetchPartnerQuery)) {

					$handyman_id = $row['handyman_id'];
					$fetchPartnerInfoQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_user` where user_id = '$handyman_id'");

					while ($res =mysqli_fetch_assoc($fetchPartnerInfoQuery)) {
							$user_email = $res['user_email'];
							$user_pic = "http://192.168.43.65:8085/ServiceYou/images/profilepic/" . $res['user_pic'];
					}

					array_push($response, 
							array(
							'conversation_id'=>$row['conversation_id'], 
							'conversation_date' =>$row['conversation_date'],
							'conversation_status'=>$row['conversation_status'], 
							'conversation_partner' =>$user_email,
							'conversation_picture' => $user_pic
							)
						);
			}
		}

	}

	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="fetchMessages"){
	$conversation_id = $_POST['conversation_id'];
	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];

	$response = array();

	$fetchMessageFirstQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_messages` WHERE `conversation_id` = '$conversation_id'");

	if($fetchMessageFirstQuery){
		$updateMessage =  mysqli_query($mysqli,"UPDATE `tbl_messages` SET `message_status`='Seen' WHERE `conversation_id`='$conversation_id'AND`recepient_id`='$user_id'");

		if($updateMessage){
			while ($row = mysqli_fetch_assoc($fetchMessageFirstQuery)) {
				$sender_id = $row['sender_id'];
				$recepient_id = $row['recepient_id'];

				$fetchSenderQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_user`
				where `user_id` = '$sender_id'");

				while ($res = mysqli_fetch_assoc($fetchSenderQuery)) {
					$sender = $res['user_fname'];
				}

				array_push($response, 
							array(
							'message_id'=>$row['message_id'], 
							'recepient_id' =>$row['recepient_id'],
							'sender_id'=>$row['sender_id'], 
							'message_sender' =>$sender,
							'message' => $row['message']
							)
						);
			}
		}
		else{
			$result["value"] = "0";
		    $result["message"] = "Error at second query ! ".mysqli_error($mysqli);
		    echo json_encode($result);
		    mysqli_close($mysqli);
		}
	}

	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="sendMessage"){
	$messages = $_POST['messages'];
	$email = $_POST['email'];	
	$sender_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];	
	$today = date("Y-m-d H:i:s");

	$fetchPartnerQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE `user_email` = '$email'");

	if($fetchPartnerQuery){
		while ($row = mysqli_fetch_assoc($fetchPartnerQuery)) {
			$recepient_id = $row['user_id'];
		}
		if($user_role=='Handyman'){
			$fetchConvo = mysqli_query($mysqli,"SELECT * FROM `tbl_conversation` WHERE `handyman_id` = '$sender_id' AND `customer_id` = '$recepient_id'");
			if(mysqli_num_rows($fetchConvo)>0){
				while ($row = mysqli_fetch_assoc($fetchConvo)) {
					$conversation_id = $row['conversation_id'];
					$customer_isActive = $row['customer_isActive'];
					if($customer_isActive==0){
						$status="Sent";
					}
					else{
						$status="Seen";
					}
				}
			$insertMessage = mysqli_query($mysqli,"INSERT INTO `tbl_messages`(`conversation_id`, `recepient_id`, `sender_id`, `message`, `date`, `message_status`, `message_isActive`) VALUES ('$conversation_id','$recepient_id','$sender_id','$messages','$today','$status','TRUE')");
			}

			else{
				$insertConvo = mysqli_query($mysqli,"INSERT INTO `tbl_conversation`(`handyman_id`, `customer_id`, `conversation_date`, `conversation_status`, `conversation_isActive`,`customer_isActive`,`handyman_isActive`) VALUES ('$sender_id','$recepient_id','$today','Published','TRUE',0,0)");


				if($insertConvo ){
					$conversation_id = mysqli_insert_id($mysqli);
				}
				else{
					$result["value"] = "0";
				    $result["message"] = "Error at inserting convo query ! ".mysqli_error($mysqli);
				    echo json_encode($result);
				    mysqli_close($mysqli);
				}

			$insertMessage = mysqli_query($mysqli,"INSERT INTO `tbl_messages`(`conversation_id`, `recepient_id`, `sender_id`, `message`, `date`, `message_status`, `message_isActive`) VALUES ('$conversation_id','$recepient_id','$sender_id','$messages','$today','Sent','TRUE')");
			}

		}

		else{
			$fetchConvo = mysqli_query($mysqli,"SELECT * FROM `tbl_conversation` WHERE `handyman_id` = '$recepient_id' AND `customer_id` = '$sender_id'");
			if(mysqli_num_rows($fetchConvo)>0){
				while ($row = mysqli_fetch_assoc($fetchConvo)) {
					$conversation_id = $row['conversation_id'];
					$handyman_isActive = $row['handyman_isActive'];
					if($handyman_isActive==0){
						$status="Sent";
					}
					else{
						$status="Seen";
					}
				}
			$insertMessage = mysqli_query($mysqli,"INSERT INTO `tbl_messages`(`conversation_id`, `recepient_id`, `sender_id`, `message`, `date`, `message_status`, `message_isActive`) VALUES ('$conversation_id','$recepient_id','$sender_id','$messages','$today','$status','TRUE')");

			}
			else{
				$insertConvo = mysqli_query($mysqli,"INSERT INTO `tbl_conversation`(`handyman_id`, `customer_id`, `conversation_date`, `conversation_status`, `conversation_isActive`,`customer_isActive`,`	handyman_isActive`) VALUES ('$sender_id','$recepient_id','$today','Published','TRUE',0,0)");
				if($insertConvo){
					$conversation_id = mysqli_insert_id($mysqli);
				}
				else{
					$result["value"] = "0";
				    $result["message"] = "Error at inserting convo query ! ".mysqli_error($mysqli);
				    echo json_encode($result);
				    mysqli_close($mysqli);
				}
			$insertMessage = mysqli_query($mysqli,"INSERT INTO `tbl_messages`(`conversation_id`, `recepient_id`, `sender_id`, `message`, `date`, `message_status`, `message_isActive`) VALUES ('$conversation_id','$recepient_id','$sender_id','$messages','$today','Sent','TRUE')");
			}		
			
		}

		if($insertMessage){
			$response["value"] = "1";
			$response["message"] = "Sent Succesfully";
		}

		else{
			$result["value"] = "0";
	    $result["message"] = "Error at second query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
		}
	}

	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="fetchPointsUpdate"){
	$user_role = $_POST['user_role'];
	$user_id = $_POST['user_id'];
	$response = array();

	$fetchPointsUpdateQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_pointsupdate` WHERE user_id = '$user_id'");
	
	if($fetchPointsUpdateQuery){
		while ($row = mysqli_fetch_assoc($fetchPointsUpdateQuery)) {
			array_push($response, 
				array(
				'points_update_id'=>$row['points_update_id'], 
				'user_id' => $row['user_id'],
				'points_amount' => $row['points_amount'],
				'points_update_date'=>$row['points_update_date'], 
				'points_update_type' => $row['points_update_type'],
				'points_update_status'=>$row['points_update_status']
				)
			);
		}		
	}
	
	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="getAvailablePoints"){
	$user_role = $_POST['user_role'];
	$user_id = $_POST['user_id'];
	$response = array();

	$fetchPointsUpdateQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE user_id = '$user_id'");
	
	if($fetchPointsUpdateQuery){
		while ($row = mysqli_fetch_assoc($fetchPointsUpdateQuery)) {
			$response["value"] = "1";
			$response["total_points"] = $row['user_points'];
			$response["message"] = "Fetch Succesfully";
		}		
	}
	
	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}

	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="rescheduleTransaction"){
	$ti_id = $_POST['ti_id'];
	$user_id = $_POST['user_id'];
	$resDate = $_POST['date'];
	$newformat = date('Y-m-d H:i:s',strtotime($resDate));
	$today = date("Y-m-d H:i:s");

	$checkTransactionQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` INNER JOIN `tbl_transaction` on tbl_transactioninfo.ti_id = tbl_transaction.ti_id WHERE tbl_transactioninfo.ti_id ='$ti_id'");

		if($checkTransactionQuery)
		{
			while ($row = mysqli_fetch_assoc($checkTransactionQuery)) {
					$transaction_id = $row['transaction_id'];
				}

			$insertReschedTransactionQuery = mysqli_query($mysqli,"INSERT INTO `tbl_rescheduledtransac` 
					(`transaction_id`, `rt_by`, `rt_date`, `rt_proposed_date`, `reasons_id`, `rt_status`, `rt_isActive`) VALUES 
					('$transaction_id','$user_id','$today','$newformat',3,'Pending','TRUE')");

			$rt_id = mysqli_insert_id($mysqli);

			if($insertReschedTransactionQuery)
			{
				$reschedTransactionQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_date`= '$newformat',`task_status` = 'Rescheduled' WHERE  `ti_id`='$ti_id'");

				if($reschedTransactionQuery)
				{
					$response["value"] = "1";
					$response["rt_id"] = $rt_id;
					$response["message"] = "Updated Succesfully";
				}
				else
				{
					$result["value"] = "0";
					$result["message"] = "Error at third query ! ".mysqli_error($mysqli);
					echo json_encode($result);
					mysqli_close($mysqli);
				}
			}
			else
			{
				$result["value"] = "0";
				$result["message"] = "Error at second query ! ".mysqli_error($mysqli);
				echo json_encode($result);
				mysqli_close($mysqli);
			}
		 	

		}
		else
		{
			$result["value"] = "0";
			$result["message"] = "Error at first query ! ".mysqli_error($mysqli);
			echo json_encode($result);
			mysqli_close($mysqli);
		}
	echo json_encode($response);
	mysqli_close($mysqli);
}

else if($key=="checkTransactionAvailabilityforHM"){
	$ti_id = $_POST['ti_id'];
	$user_id = $_POST['user_id'];
	$checkTransactionInfoQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` WHERE `ti_id`='$ti_id'");

	if($checkTransactionInfoQuery)
	{
		if(mysqli_num_rows($checkTransactionInfoQuery)>0)
		{
			while ($row = mysqli_fetch_assoc($checkTransactionInfoQuery)) {
				$status = $row['task_status'];
				$handyman_id = $row['handyman_id'];
				$task_date = $row['task_date'];
			}

			if($status=='Accepted'&&$handyman_id==$user_id)
			{
			$response["value"] = "1";
			$response["message"] = "Still Available";
			}

			else if($status=='Cancelled'&&$handyman_id==$user_id)
			{
			$response["value"] = "2";
			$response["message"] = "Cancelled";
			}

			else if($status=='Rescheduled'&&$handyman_id==$user_id)
			{
				$checkTransactionQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transaction` INNER JOIN `tbl_rescheduledtransac` on tbl_transaction.transaction_id = tbl_rescheduledtransac.transaction_id WHERE tbl_transaction.ti_id='$ti_id' AND tbl_rescheduledtransac.rt_status='Pending'");

					if(mysqli_num_rows($checkTransactionQuery)>0)
					{
						while ($row = mysqli_fetch_assoc($checkTransactionQuery)) {
							$transaction_id = $row['transaction_id'];
							$rt_id = $row['rt_id'];
							$rt_proposed_date = $row['rt_proposed_date'];							
							$rt_date = $row['rt_date'];
						}
						$response["value"] = "3";
						$response["transaction_id"] = $transaction_id;
						$response["rt_id"] = $rt_id;
						$response["rt_proposed_date"] = $rt_proposed_date;
						$response["rt_date"] = $rt_date;
					}
					else
					{
						$result["value"] = "0";
						$result["message"] = "Error at third query ! ".mysqli_error($mysqli);
						echo json_encode($result);
						mysqli_close($mysqli);
					}
			}

		}

		else
		{
			$result["value"] = "0";
			$result["message"] = "Error at second query ! ".mysqli_error($mysqli);
			echo json_encode($result);
			mysqli_close($mysqli);
		}

	}

	else
	{
		$result["value"] = "0";
		$result["message"] = "Error at first query ! ".mysqli_error($mysqli);
		echo json_encode($result);
		mysqli_close($mysqli);
	}

	echo json_encode($response);
	mysqli_close($mysqli);
}

else if($key=="checkRescheduledTransaction"){
	$ti_id = $_POST['ti_id'];
	$user_id = $_POST['user_id'];
	$rt_id = $_POST['rt_id'];
	$checkRescheduledTransactionQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` INNER JOIN `tbl_transaction` on tbl_transactioninfo.ti_id = tbl_transaction.ti_id INNER JOIN `tbl_rescheduledtransac` on tbl_transaction.transaction_id = tbl_rescheduledtransac.transaction_id where tbl_rescheduledtransac.rt_id = $rt_id AND tbl_transactioninfo.customer_id = $user_id");

	if($checkRescheduledTransactionQuery)
	{
		if(mysqli_num_rows($checkRescheduledTransactionQuery)>0)
		{
			while ($row = mysqli_fetch_assoc($checkRescheduledTransactionQuery)) {
				$rt_status = $row['rt_status'];
			}

			if($rt_status=='Pending')
			{
			$response["value"] = "1";			
			$response["rt_status"] = $rt_status;
			$response["message"] = "Pending";
			}

			else if($rt_status=='Published')
			{
			$response["value"] = "2";			
			$response["rt_status"] = $rt_status;
			$response["message"] = "Accepted";
			}
		}

		else if(mysqli_num_rows($checkRescheduledTransactionQuery)==0)
		{
			$response["value"] = "3";
			$response["message"] = "Cancelled";
		}	

		else
		{
			$result["value"] = "0";
			$result["message"] = "Error at second query ! ".mysqli_error($mysqli);
			echo json_encode($result);
			mysqli_close($mysqli);
		}

	}

	else
	{
		$result["value"] = "0";
		$result["message"] = "Error at first query ! ".mysqli_error($mysqli);
		echo json_encode($result);
		mysqli_close($mysqli);
	}

	echo json_encode($response);
	mysqli_close($mysqli);
}

else if($key=="updateRescheduledStatus"){
	$rt_id = $_POST['rt_id'];
	$updateRescheduledStatusQuery = mysqli_query($mysqli,"UPDATE `tbl_rescheduledtransac` SET `rt_status`='Published' WHERE `rt_id` = '$rt_id'");

	if($updateRescheduledStatusQuery)
	{
		$response["value"] = "1";			
		$response["message"] = "Success";

	}

	else
	{
		$result["value"] = "0";
		$result["message"] = "Error at first query ! ".mysqli_error($mysqli);
		echo json_encode($result);
		mysqli_close($mysqli);
	}

	echo json_encode($response);
	mysqli_close($mysqli);
}

else if($key=="deleteRescheduledStatus"){
	$rt_id = $_POST['rt_id'];
	$user_id = $_POST['user_id'];
	$ti_id = $_POST['ti_id'];
	$transaction_id = $_POST['transaction_id'];
	$today = date("Y-m-d H:i:s");

	$deleteRescheduledStatusQuery = mysqli_query($mysqli,"DELETE FROM `tbl_rescheduledtransac` WHERE `rt_id` = '$rt_id'");

	if($deleteRescheduledStatusQuery)
	{
		$updateTransactionInfoQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Cancelled' WHERE `ti_id` = $ti_id");
		if($updateTransactionInfoQuery){
			$insertCancelledTransactionInfoQuery = mysqli_query($mysqli," INTO `tbl_cancelledtransac`
				(`transaction_id`, `ct_date`, `reasons_id`, `ct_status`, `ct_isActive`,`ct_by`) VALUES 
				('$transaction_id','$today',4,'Published','TRUE','$user_id')");

			if($insertCancelledTransactionInfoQuery)
			{				
				$response["value"] = "1";			
				$response["message"] = "Success";	
			}
			else
			{
				$result["value"] = "0";
				$result["message"] = "Error at third query ! ".mysqli_error($mysqli);
				echo json_encode($result);
				mysqli_close($mysqli);
			}
		}
		else{
			$result["value"] = "0";
			$result["message"] = "Error at second query ! ".mysqli_error($mysqli);
			echo json_encode($result);
			mysqli_close($mysqli);
		}

	}

	else
	{
		$result["value"] = "0";
		$result["message"] = "Error at first query ! ".mysqli_error($mysqli);
		echo json_encode($result);
		mysqli_close($mysqli);
	}

	echo json_encode($response);
	mysqli_close($mysqli);
}

else if($key=="hmTracker"){
	$ti_id = $_POST['ti_id'];
	$customer_id = $_POST['customer_id'];

	$hmArrivalTrackerQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` WHERE `ti_id`='$ti_id' && `customer_id` = '$customer_id'");


	if($hmArrivalTrackerQuery){	
		while ($row = mysqli_fetch_assoc($hmArrivalTrackerQuery)) {
			$task_status = $row['task_status'];
		}
		if($task_status=='Pending Arrival'){
			$result["value"] = "1";
		    $result["message"] = "Pending Arrival";			
		}
		else if($task_status=='Service in Progress'){
			$result["value"] = "2";
		    $result["message"] = "Service in Progress";
		}
		else if($task_status=='Pending Completion Confirmation'){
			$result["value"] = "3";
		    $result["message"] = "Pending Completion Confirmation";
		}
		else{
			$result["value"] = "0";
		    $result["message"] = "On the Way";
		}
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="confirmationTracker"){
	$warranty_id = $_POST['warranty_id'];
	$ti_id = $_POST['ti_id'];
	$handyman_id = $_POST['handyman_id'];

	$confirmationTrackerTrackerQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` WHERE `ti_id`='$ti_id' && `handyman_id` = '$handyman_id'");

	if($confirmationTrackerTrackerQuery){	

		while ($row = mysqli_fetch_assoc($confirmationTrackerTrackerQuery)) {
			$task_status = $row['task_status'];
		}

		if($task_status=='Pending Arrival'){
			$result["value"] = "1";
		    $result["message"] = "Pending Arrival";			
		}

		else if($task_status=='Service in Progress'){
			$result["value"] = "2";
		    $result["message"] = "Service in Progress";
		}

		else if($task_status=='Pending Completion Confirmation'){
			$result["value"] = "3";
		    $result["message"] = "Pending Completion Confirmation";
		}

		else if($task_status=='Completed'){
			if($warranty_id==0){
				$selectTransactionInfo = mysqli_query($mysqli,"SELECT * FROM `tbl_transaction` INNER JOIN `tbl_transactioninfo` 
				ON tbl_transaction.ti_id = tbl_transactioninfo.ti_id INNER JOIN `tbl_subservice`
				ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id  WHERE tbl_transactioninfo.ti_id='$ti_id' && tbl_transactioninfo.handyman_id = '$handyman_id'");

				while ($row = mysqli_fetch_assoc($selectTransactionInfo)) {
						$result["transaction_id"] = $row['transaction_id'];
						$result["subservice"] = $row['subservice'];
						$result["promo_id"] = $row['promo_id'];
						$result["transaction_earnings"] = $row['transaction_earnings'];
						$result["transaction_date"] = $row['transaction_date'];
						$response["customer_id"] = $row['customer_id'];
						}

				$result["value"] = "4";
			    $result["message"] = "Completed";
			}
			else{				
				$result["value"] = "6";
			    $result["message"] = "Completed Warranty";
			}
		}

		else if($task_status=='Accepted'){
			$result["value"] = "5";
		    $result["message"] = "Declined Arrival";
		}
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="setHandymanArrival"){
	$ti_id = $_POST['ti_id'];
	$customer_id = $_POST['customer_id'];

	$setHandymanArrivalQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Service in Progress' WHERE `ti_id`='$ti_id' && `customer_id` = '$customer_id'");

	if($setHandymanArrivalQuery){	
		$result["value"] = "1";
	    $result["message"] = "Service in Progress";
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="cancelHandymanArrival"){
	$ti_id = $_POST['ti_id'];
	$customer_id = $_POST['customer_id'];

	$cancelHandymanArrivalQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Accepted' WHERE `ti_id`='$ti_id' && `customer_id` = '$customer_id'");

	if($cancelHandymanArrivalQuery){	
		$result["value"] = "1";
	    $result["message"] = "Handyman not arrived yet";
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="setHandymanCompletion"){
	$warranty_id = $_POST['warranty_id'];
	$ti_id = $_POST['ti_id'];
	$customer_id = $_POST['customer_id'];
	$today = date("Y-m-d H:i:s");

	if($warranty_id==0){
		$setHandymanCompletionQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Completed' WHERE `ti_id`='$ti_id' && `customer_id` = '$customer_id'");

		if($setHandymanCompletionQuery){
				$selectTransactionInfo = mysqli_query($mysqli,"SELECT * FROM `tbl_transaction` INNER JOIN `tbl_transactioninfo` 
				ON tbl_transaction.ti_id = tbl_transactioninfo.ti_id INNER JOIN `tbl_subservice`
				ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id  WHERE tbl_transactioninfo.ti_id='$ti_id' && tbl_transactioninfo.customer_id = '$customer_id'");
				
				if($selectTransactionInfo)
				{
					if(mysqli_num_rows($selectTransactionInfo)>0){
						while ($row = mysqli_fetch_assoc($selectTransactionInfo)) {
						$transaction_id = $row['transaction_id'];
						$subservice_rate = $row['subservice_rate'];
						$subservice = $row['subservice'];
						$handyman_id = $row['handyman_id'];
						$customer_id = $row['customer_id'];
						$promo_id = $row['promo_id'];
						$transaction_date = $row['transaction_date'];
						}
						if($promo_id!=0){
							$selectPromoInfo = mysqli_query($mysqli,"SELECT * FROM `tbl_promos` WHERE `promo_id` ='$promo_id'");
							if(mysqli_num_rows($selectPromoInfo)>0){
								while ($row = mysqli_fetch_assoc($selectPromoInfo)) {
								$promo_amount = $row['promo_amount'];							
								$promo_price = $row['promo_price'];
								$promo_type = $row['promo_type'];
								}
								if($promo_type=='Discount'){
									$earnings = $subservice_rate - $promo_amount;
									$commission = $earnings * .10;
								}
								else{
									$earnings = $subservice_rate - ($subservice_rate * ($promo_amount/100));
									$commission = $earnings * .10;
								}
							}
						}
						else{
							$earnings = $subservice_rate;
							$commission = $earnings * .10;
						}
						$updateTransaction = mysqli_query($mysqli,"UPDATE `tbl_transaction` SET `transaction_earnings`='$earnings',`transaction_commission`='$commission' WHERE `transaction_id` = '$transaction_id'");
						if($updateTransaction){
							$selectHandyman = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE `user_id` = '$handyman_id'");
							$selectCustomer = mysqli_query($mysqli,"SELECT * FROM `tbl_user` WHERE `user_id` = '$customer_id'");

							if($selectHandyman&&$selectCustomer){
								while ($row = mysqli_fetch_assoc($selectHandyman)) {
								$user_points = $row['user_points'];	
									if($promo_id!=0){
										if($promo_type=='Discount'){
											$handyman_updatedPoints = $user_points + $promo_amount;	
											$points_amount = $promo_amount;
										}
										else{
											$handyman_updatedPoints = $user_points + ($subservice_rate * ($promo_amount/100));
											$points_amount = ($subservice_rate * ($promo_amount/100));
										}
										$point_update_type = 'Addition-Points';	
									}
									else{
										$handyman_updatedPoints = $user_points - $commission;
										$points_amount = $commission;
										$point_update_type = 'Deduction-Points';	
									}
									$updateHandymanPoints = mysqli_query($mysqli,"UPDATE `tbl_user` SET `user_points`='$handyman_updatedPoints' WHERE `user_id` = '$handyman_id'");
								}

								$insertHandymanPointsUpdate = mysqli_query($mysqli,"INSERT INTO `tbl_pointsupdate`
									(`user_id`, `points_amount`, `points_update_date`, `points_update_type`, `points_update_status`) VALUES 
									('$handyman_id','$points_amount','$today','$point_update_type','Published')");

								while ($row = mysqli_fetch_assoc($selectCustomer)) {
									$user_points = $row['user_points'];
									if($promo_id!=0){
										$customer_updatedPoints = $user_points - $promo_price;
										$points_amount = $promo_price;
										$point_update_type = 'Deduction-Points';	
									}
									else{
										$customer_updatedPoints = $user_points + $commission;
										$points_amount = $commission;
										$point_update_type = 'Addition-Points-Points';	
									}					
									$updateCustomerPoints = mysqli_query($mysqli,"UPDATE `tbl_user` SET `user_points`='$customer_updatedPoints' WHERE `user_id` = '$customer_id'");
								}

								$insertCustomerPointsUpdate = mysqli_query($mysqli,"INSERT INTO `tbl_pointsupdate`
									(`user_id`, `points_amount`, `points_update_date`, `points_update_type`, `points_update_status`) VALUES 
									('$customer_id','$points_amount','$today','$point_update_type','Published')");

								$countCustomerServiceRendered = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` WHERE `customer_id` = '$customer_id' && `task_status` = 'Completed'");

								if(mysqli_num_rows($countCustomerServiceRendered)>=10&&mysqli_num_rows($countCustomerServiceRendered)<20){
									$updateCustomerBadge = mysqli_query($mysqli,"UPDATE `tbl_customerprofile` SET `user_badge`='Silver' WHERE `user_id` = '$customer_id'");
									$response["message"] = "Upgraded to Silver";
								}
								else if(mysqli_num_rows($countCustomerServiceRendered)>=20){
									$updateCustomerBadge = mysqli_query($mysqli,"UPDATE `tbl_customerprofile` SET `user_badge`='Gold' WHERE `user_id` = '$customer_id'");
									$response["message"] = "Upgraded to Gold";
								}
								else{
									$response["message"] = "Success";
								}

								if($insertHandymanPointsUpdate&&$insertCustomerPointsUpdate){
									$insertIntoCompletedTransac = mysqli_query($mysqli,"INSERT INTO `tbl_completedtransac`(`transaction_id`, `cot_date`, `cot_status`, `cot_isActive`) VALUES ('$transaction_id','$today','Published','TRUE')");
									$date = date("Y-m-d H:i:s", strtotime(" +30 days"));

									$insertIntoWarranty = mysqli_query($mysqli,"INSERT INTO `tbl_warranty`(`transaction_id`, `warranty_status`, `warranty_expiration`, `warranty_isActive`,`changes_isSeen_byCust`,`changes_isSeen_byHandy`) VALUES ('$transaction_id','Unused','$date',1,0,0)");

									$response["value"] = "1";			
									$response["transaction_earnings"] = $earnings;									
									$response["transaction_id"] = $transaction_id;	
									$response["subservice"] = $subservice;
									$response["transaction_date"] = $transaction_date;			
									$response["promo_id"] = $promo_id;
									$response["handyman_id"] = $handyman_id;
								}
								else{
									$result["value"] ="0";
									$result["message"] ="Error at fifth query null".mysqli_error($mysqli);
									json_encode($result);
									mysqli_close($mysqli);
								}

							}
						}
						else{
							$result["value"] ="0";
							$result["message"] ="Error at fourth query null".mysqli_error($mysqli);
							json_encode($result);
							mysqli_close($mysqli);
						}

					}
					else{
						$result["value"] ="0";
						$result["message"] ="Error at third query null".mysqli_error($mysqli);
						json_encode($result);
						mysqli_close($mysqli);
					}
				}

				else{
					$result["value"] ="0";
					$result["message"] ="Error at second query".mysqli_error($mysqli);
					json_encode($result);
					mysqli_close($mysqli);
				}
			
		}
		else{
			$result["value"] ="0";
			$result["message"] ="Error at first query".mysqli_error($mysqli);
			json_encode($result);
			mysqli_close($mysqli);
		}
	}
	else{
		$setHandymanCompletionQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Completed' WHERE `ti_id`='$ti_id' && `customer_id` = '$customer_id'");
		$setWarrantyQuery = mysqli_query($mysqli,"UPDATE `tbl_warranty` SET `warranty_status`='Used' WHERE `warranty_id`='$warranty_id'");
		if($setHandymanCompletionQuery&&$setWarrantyQuery){
			$response["value"] = "2";	
			$response["message"] = "Success";
		}
		else{
			$result["value"] ="0";
			$result["message"] ="Error at first query".mysqli_error($mysqli);
			json_encode($result);
			mysqli_close($mysqli);
		}
	}


		echo json_encode($response);
	    mysqli_close($mysqli);
}

else if($key=="cancelHandymanCompletion"){
	$ti_id = $_POST['ti_id'];
	$customer_id = $_POST['customer_id'];

	$cancelHandymanCompletionQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Service in Progress' WHERE `ti_id`='$ti_id' && `customer_id` = '$customer_id'");

	if($cancelHandymanCompletionQuery){	
		$result["value"] = "1";
	    $result["message"] = "Handyman not finished yet";
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="updateArrival"){
	$ti_id = $_POST['ti_id'];
	$handyman_id = $_POST['handyman_id'];

	$updateArrivalQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Pending Arrival' WHERE `ti_id`='$ti_id' && `handyman_id` = '$handyman_id'");

	if($updateArrivalQuery){	
		$result["value"] = "1";
	    $result["message"] = "Pending Arrival";
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="updateCompletion"){
	$ti_id = $_POST['ti_id'];
	$handyman_id = $_POST['handyman_id'];

	$updateCompletionQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Pending Completion Confirmation' WHERE `ti_id`='$ti_id' && `handyman_id` = '$handyman_id'");

	if($updateCompletionQuery){	
		$result["value"] = "1";
	    $result["message"] = "Pending Completion Confirmation";
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="rate"){
	$key = $_POST['key'];
	$rate = $_POST['rate'];
	$reviews = $_POST['reviews'];
	$rater_id = $_POST['rater_id'];
	$user_id = $_POST['user_id'];
	$transaction_id = $_POST['transaction_id'];	
	$today = date("Y-m-d H:i:s");

	$insertRateQuery = mysqli_query($mysqli,"INSERT INTO `tbl_ratesandreview`
		(`user_id`, `rater_id`, `transaction_id`, `rating_date`, `reviews`, `ratings`, `ratings_status`, `rating_isActive`) VALUES 
		('$user_id','$rater_id','$transaction_id','$today','$reviews','$rate','Published','TRUE')");

	if($insertRateQuery){
		$result["value"] = "1";
	    $result["message"] = "Success";
	}

	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="sendReport"){
	$report = $_POST['report'];
	$user_id = $_POST['user_id'];	
	$today = date("Y-m-d H:i:s");

	$insertReportQuery = mysqli_query($mysqli,"INSERT INTO `tbl_reports`(`user_id`, `report`, `report_status`, `report_date`, `report_isActive`) VALUES ('$user_id','$report','Sent','$today','TRUE')");

	if($insertReportQuery){
		$result["value"] = "1";
	    $result["message"] = "Success";
	}

	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="fetchTransactionInfo"){
	$transaction_id = $_POST['transaction_id'];
    $fetch_transaction = mysqli_query($mysqli, "SELECT *
                FROM tbl_transaction
                INNER JOIN tbl_transactioninfo ON tbl_transaction.ti_id = tbl_transactioninfo.ti_id
                INNER JOIN tbl_subservice ON tbl_transactioninfo.subservice_id = tbl_subservice.subservice_id
                INNER JOIN tbl_service ON tbl_subservice.service_id = tbl_service.service_id
                where tbl_transaction.transaction_id = $transaction_id");

	if($fetch_transaction){
		while($row = mysqli_fetch_array($fetch_transaction))
		    {
		    $result["transaction_id"] = $row['transaction_id'];
		    $result["transaction_date"] = $row['transaction_date'];
		    $result["transaction_earnings"] = $row['transaction_earnings'];
		    $result["transaction_commission"] = $row['transaction_commission'];
		    $result["promo_id"] = $row['promo_id'];
		    $result["subservice"] = $row['subservice'];
		    $result["service"] = $row['service'];
		    $result["ti_id"] = $row['ti_id'];
		    $result["task_date"] = $row['task_date'];
		    $result["task_location"] = $row['task_location'];
		    $result["task_instruction"] = $row['task_instruction'];
		    $result["task_status"] = $row['task_status'];
		    $customer_id = $row['customer_id'];
		    $handyman_id = $row['handyman_id'];

		     $fetch_customer = mysqli_query($mysqli, "SELECT *
                FROM tbl_user WHERE user_id = $customer_id");
			    while($cust_row = mysqli_fetch_array($fetch_customer))
			    {
			      $result['customer_name'] = $cust_row['user_fname'].' '.$cust_row['user_lname'];
			    }

			    $fetch_handyman = mysqli_query($mysqli, "SELECT *
			                FROM tbl_user WHERE user_id = $handyman_id");
			    while($handy_row = mysqli_fetch_array($fetch_handyman))
			    {
			      $result['handyman_name'] = $handy_row['user_fname'].' '.$handy_row['user_lname'];
			    }
		    }

		$result["value"] = "1";
	    $result["message"] = "Success";
	}

	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="checkTransaction"){
	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];
	if($user_role=='Handyman'){
		$filter="handyman_id";
	}
	else{
		$filter="customer_id";
	}


	$checkTransaction = mysqli_query($mysqli, "SELECT * FROM `tbl_transactioninfo` WHERE $filter='$user_id' AND `task_status`='Accepted' OR `task_status`='Pending Arrival' OR `task_status`='Service in Progress' OR `task_status`='Pending Completion Confirmation'");
	if($checkTransaction&&mysqli_num_rows($checkTransaction)>0){
	while($row = mysqli_fetch_array($checkTransaction))
		    {
		    $result["ti_id"] = $row['ti_id'];
		    $result["service_id"] = $row['service_id'];
		    $result["subservice_id"] = $row['subservice_id'];
		    $result["task_type"] = $row['task_type'];
		    $result["task_date"] = $row['task_date'];
		    $result["task_location"] = $row['task_location'];
		    $result["task_instruction"] = $row['task_instruction'];		    
		    $result["longitude"] = $row['longitude'];
		    $result["latitude"] = $row['latitude'];
		    $result["task_status"] = $row['task_status'];		    
		    $result["customer_id"] = $row['customer_id'];		
		    $result["handyman_id"] = $row['handyman_id'];
		    $handyman_id = $row['handyman_id'];
			$customer_id = $row['customer_id'];
			$selectConvo =  mysqli_query($mysqli,"SELECT * FROM `tbl_conversation` WHERE customer_id='$customer_id' AND handyman_id='$handyman_id'");
			
			if(mysqli_num_rows($selectConvo)>0){
			while ($row = mysqli_fetch_assoc($selectConvo)) {
				$result["conversation_id"] = $row['conversation_id'];
			}

			}
			else{
				$insertConvo =  mysqli_query($mysqli,"INSERT INTO `tbl_conversation`(`handyman_id`, `customer_id`, `conversation_date`, `conversation_status`, `conversation_isActive`) VALUES ('$handyman_id','$customer_id','$today','Published','TRUE')");
				$result["conversation_id"] = mysqli_insert_id($mysqli);
			}

			$selectCustomer =  mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` INNER JOIN `tbl_user` ON tbl_transactioninfo.customer_id = tbl_user.user_id WHERE tbl_user.user_id = '$customer_id'");

			if($selectCustomer){
				while ($row = mysqli_fetch_assoc($selectCustomer)) {
					$result["user_fname"] = $row['user_fname'];
					$result["user_lname"] = $row['user_lname'];
					$result["user_email"] = $row['user_email']; 
					$result["task_location"] = $row['task_location'];
				}
				$result["value"] = "1";	
			    $result["message"] = "Transaction Updated";
			}
		}
		$result["value"] = "1";
	    $result["message"] = "Check Handyman Transaction";
	}

	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="onlineUser"){

	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];
	$conversation_id = $_POST['conversation_id'];

	if($user_role=='Handyman'){
		$filter="handyman_isActive";
		$filerId="handyman_id";
	}
	else{
		$filter="customer_isActive";
		$filerId="customer_id";
	}

	$updateAvailability = mysqli_query($mysqli, "UPDATE `tbl_conversation` SET $filter=1 WHERE $filerId='$user_id' AND conversation_id='$conversation_id'");

	if($updateAvailability){
		$result["value"] = "1";
	    $result["message"] = "Success";
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="offlineUser"){

	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];
	$conversation_id = $_POST['conversation_id'];

	if($user_role=='Handyman'){
		$filter="handyman_isActive";
		$filerId="handyman_id";
	}
	else{
		$filter="customer_isActive";
		$filerId="customer_id";
	}

	$updateAvailability = mysqli_query($mysqli, "UPDATE `tbl_conversation` SET $filter=0 WHERE $filerId='$user_id' AND conversation_id='$conversation_id'");

	if($updateAvailability){
		$result["value"] = "1";
	    $result["message"] = "Success";
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="getMessages"){
	$user_id = $_POST['user_id'];
	$response = array();

	$fetchUser=mysqli_query($mysqli,"SELECT * FROM `tbl_user` where `user_id` = '$user_id'");
	if($fetchUser){
		while ($row = mysqli_fetch_assoc($fetchUser)) {
			$user_role = $row['user_role'];
		}

		if($user_role=="Handyman"){
			$filter = "handyman_id";
		}

		else{
			$filter = "customer_id";
		}

		$getMessages =mysqli_query($mysqli,"SELECT * FROM `tbl_messages` INNER JOIN `tbl_conversation` on tbl_messages.conversation_id = tbl_conversation.conversation_id WHERE tbl_messages.recepient_id='$user_id' AND tbl_messages.message_status='Sent'");

		if($getMessages){
				while ($row = mysqli_fetch_assoc($getMessages)) {
					$conversation_id = $row['conversation_id'];
					$sender_id = $row['sender_id'];
					$recepient_id = $row['recepient_id'];
					$fetchSenderQuery = mysqli_query($mysqli,"SELECT * FROM `tbl_user`
					where `user_id` = '$sender_id'");
					while ($res = mysqli_fetch_assoc($fetchSenderQuery)) {
					$sender = $res['user_fname'];
					}
			array_push($response, 
						array(
						'message_id'=>$row['message_id'], 
						'recepient_id' =>$row['recepient_id'],
						'sender_id'=>$row['sender_id'], 
						'message_sender' =>$sender,
						'conversation_id' => $conversation_id,						
						'message' => $row['message']
						)
					);
			}
		}
		else{

		}
	}
	else{
		$result["value"] = "0";
	    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
	    echo json_encode($result);
	    mysqli_close($mysqli);
	}
	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="getWarranty"){
	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];
	$response = array();
	$today = date("Y-m-d H:i:s");

		if($user_role=="Handyman"){
			$filter = "handyman_id";
		}

		else{
			$filter = "customer_id";
		}

		$getWarranty=mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` INNER JOIN `tbl_transaction` 
			ON tbl_transactioninfo.ti_id = tbl_transaction.ti_id INNER JOIN `tbl_warranty` 
			ON tbl_transaction.transaction_id = tbl_warranty.transaction_id INNER JOIN `tbl_warrantyinfo`
			ON tbl_warranty.warranty_id = tbl_warrantyinfo.warranty_id where $filter=$user_id AND (tbl_warranty.warranty_status!='Unused' AND tbl_warranty.warranty_status!='Scheduled' AND tbl_warranty.warranty_status!='Used' AND tbl_warranty.warranty_status!='OnGoing')");

		if($getWarranty){
			while ($row = mysqli_fetch_assoc($getWarranty)) {
			if($user_role=="Handyman"){
				$isSeen=$row['changes_isSeen_byHandy'];
			}
			else{
				$isSeen=$row['changes_isSeen_byCust'];
			}
			//$datetime2 = strtotime($row['warranty_date']);
			//$datetime1 = strtotime($today);

			//$secs = $datetime2 - $datetime1;
			//$minutes = $secs / 60;	

			if($isSeen==0){
					if($filter=="handyman_id"&&$row['warranty_status']=='Requested'){
						array_push($response, 
						array(	
							'isSeen'=>$isSeen,
						'warranty_id'=>$row['warranty_id'], 
						'warranty_instruction' =>$row['warranty_instruction'],
						'warranty_date'=>$row['warranty_date'],					
						'warranty_status' => $row['warranty_status']
						)
						);
					}
					if($filter=="customer_id"&&$row['warranty_status']=='Accepted'){
							array_push($response, 
							array(	
								'isSeen'=>$isSeen,
							'warranty_id'=>$row['warranty_id'], 
							'warranty_instruction' =>$row['warranty_instruction'],
							'warranty_date'=>$row['warranty_date'],					
							'warranty_status' => $row['warranty_status']
							)
						);
					}					
				}
			}
		}

		else{
			$result["value"] = "0";
		    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
		    echo json_encode($result);
		    mysqli_close($mysqli);
		}
	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="getRescheduled"){
	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];
	$response = array();
	$today = date("Y-m-d H:i:s");

		if($user_role=="Handyman"){
			$filter = "handyman_id";
		}

		else{
			$filter = "customer_id";
		}

		$getRescheduled  =mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` INNER JOIN `tbl_transaction` 
			ON tbl_transactioninfo.ti_id = tbl_transaction.ti_id INNER JOIN `tbl_rescheduledtransac` 
			ON tbl_transaction.transaction_id = tbl_rescheduledtransac.transaction_id where $filter=$user_id AND task_status='Rescheduled'");

		if($getRescheduled){
			while ($row = mysqli_fetch_assoc($getRescheduled)) {
			$datetime2 = strtotime($row['rt_proposed_date']);
			$datetime1 = strtotime($today);

			$secs = $datetime2 - $datetime1;
			$minutes = $secs / 60;	

			if($minutes<10){
					array_push($response, 
					array(	
					'seconds'=>$minutes,
					'ti_id'=>$row['ti_id'], 
					'task_instruction' =>$row['task_instruction']
					)
					);
				}
			}
		}

		else{
			$result["value"] = "0";
		    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
		    echo json_encode($result);
		    mysqli_close($mysqli);
		}
	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="getReserved"){
	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];
	$response = array();
	$today = date("Y-m-d H:i:s");

		if($user_role=="Handyman"){
			$filter = "handyman_id";
		}

		else{
			$filter = "customer_id";
		}

		$getReserved  =mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` where $filter=$user_id AND task_type='ForLater' AND task_status='Reserved'");

		if($getReserved){
			while ($row = mysqli_fetch_assoc($getReserved)) {
			$datetime2 = strtotime($row['task_date']);
			$datetime1 = strtotime($today);

			$secs = $datetime2 - $datetime1;
			$minutes = $secs / 60;	

			if($minutes<10){
					array_push($response, 
					array(	
					'seconds'=>$minutes,
					'ti_id'=>$row['ti_id'], 
					'task_instruction' =>$row['task_instruction']
					)
					);
				}
			}
		}

		else{
			$result["value"] = "0";
		    $result["message"] = "Error at first query ! ".mysqli_error($mysqli);
		    echo json_encode($result);
		    mysqli_close($mysqli);
		}
	echo json_encode($response);
    mysqli_close($mysqli);
}

else if($key=="updateRescheduledStatusNotification"){
	$ti_id = $_POST['ti_id'];
	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];

	if($user_role=='Handyman'){
		$filerId="handyman_id";
	}
	else{
		$filerId="customer_id";
	}

	$updateArrivalQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Accepted' WHERE `ti_id`='$ti_id' && `$filerId` = '$user_id'");

	if($updateArrivalQuery){	
		$result["value"] = "1";
	    $result["message"] = "Success";
	}
	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="updateWarrantyStatus"){
	$warranty_id = $_POST['warranty_id'];
	$user_id = $_POST['user_id'];
	$user_role = $_POST['user_role'];

		if($user_role=='Handyman'){
			$filerId="changes_isSeen_byHandy";
		}

		else{
			$filerId="changes_isSeen_byCust";
		}

	$selectTransactionInfo = mysqli_query($mysqli,"SELECT * FROM `tbl_transactioninfo` INNER JOIN `tbl_transaction` 
		ON tbl_transactioninfo.ti_id = tbl_transaction.ti_id INNER JOIN `tbl_warranty` 
		ON tbl_transaction.transaction_id = tbl_warranty.transaction_id 
		WHERE warranty_id = '$warranty_id'"); 

	if(mysqli_num_rows($selectTransactionInfo)>0){
		while ($row = mysqli_fetch_assoc($selectTransactionInfo)) {
			$ti_id = $row['ti_id'];
		}

			if($user_role=='Handyman'){
				$updateWarrantyQuery = mysqli_query($mysqli,"UPDATE `tbl_warranty` SET $filerId='TRUE' , `warranty_status` ='Accepted' WHERE `warranty_id`='$warranty_id'");
				$updateTransactionInfoQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Accepted' where `ti_id`='$ti_id'");

			}
			else{
				$updateWarrantyQuery = mysqli_query($mysqli,"UPDATE `tbl_warranty` SET $filerId='TRUE' , `warranty_status` ='OnGoing' WHERE `warranty_id`='$warranty_id'");		
				$updateTransactionInfoQuery = mysqli_query($mysqli,"UPDATE `tbl_transactioninfo` SET `task_status`='Accepted' where `ti_id`='$ti_id'");
			}

			if($updateWarrantyQuery){	
				$result["ti_id"] = $ti_id;
				$result["value"] = "1";
			    $result["message"] = "Success";
			}

			else{
				$response["value"] ="0";
				$response["message"] ="Error at first query";
				json_encode($response);
				mysqli_close($mysqli);
			}

	}

	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

else if($key=="checkHandymanAvailability"){

	$transaction_id = $_POST['transaction_id'];

	$checkHandymanAvailabilityQuery = mysqli_query($mysqli,"SELECT * FROM tbl_transactioninfo INNER JOIN tbl_transaction ON tbl_transactioninfo.ti_id = tbl_transaction.ti_id WHERE tbl_transaction.transaction_id = $transaction_id");

	if($checkHandymanAvailabilityQuery){
		if(mysqli_num_rows($checkHandymanAvailabilityQuery)>0){
			while($row = mysqli_fetch_assoc($checkHandymanAvailabilityQuery)){
				$handyman_id = $row['handyman_id'];
			}

			$checkHandymanStatusQuery = mysqli_query($mysqli,"SELECT * FROM tbl_transactioninfo WHERE `handyman_id` = $handyman_id 
				AND (`task_status` = 'Accepted' || `task_status` = 'Pending Arrival' || `task_status` = 'Service in Progress' || `task_status` = 'Pending Completion Confirmation')");

			if(mysqli_num_rows($checkHandymanStatusQuery)>0)
			{
				$result["value"] = "1";
		    	$result["message"] = "Handyman Busy";
			}

			else{
				$result["value"] = "2";
		    	$result["message"] = "Handyman Not Busy";
			}
		}
	}

	else{
		$response["value"] ="0";
		$response["message"] ="Error at first query";
		json_encode($response);
		mysqli_close($mysqli);
	}

	echo json_encode($result);
    mysqli_close($mysqli);
}

?>